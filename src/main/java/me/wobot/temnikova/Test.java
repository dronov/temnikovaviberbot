package me.wobot.temnikova;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

public class Test {

    public static final DateFormat DATE_FORMAT = new SimpleDateFormat("dd.MM.yyyy HH:mm");

    {
        DATE_FORMAT.setTimeZone(TimeZone.getTimeZone("Europe/Moscow"));
    }

    public static void main(String[] args) throws ParseException {
        Date date = DATE_FORMAT.parse("06.11.2017 21:05");
        Date now = new Date();

        long diff = date.getTime() - now.getTime();
        long hours = TimeUnit.MILLISECONDS.toHours(diff);
        System.out.println(hours);
    }

}
