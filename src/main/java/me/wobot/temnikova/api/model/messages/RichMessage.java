package me.wobot.temnikova.api.model.messages;

import me.wobot.temnikova.api.model.keyboard.Keyboard;
import me.wobot.temnikova.api.model.keyboard.RichMedia;
import org.json.JSONObject;

public class RichMessage extends AbsMessage {

    private final RichMedia richMedia;

    public RichMessage(RichMedia richMedia) {
        super(MessageType.RICH_MEDIA, null);
        this.richMedia = richMedia;
    }

    public RichMessage(RichMedia richMedia, Keyboard keyboard) {
        super(MessageType.RICH_MEDIA, keyboard);
        this.richMedia = richMedia;
    }

    @Override
    public void serialize(JSONObject json) {
        super.serialize(json);
        if (richMedia != null) {
            JSONObject rich = new JSONObject();
            richMedia.serialize(rich);
            json.put("rich_media", rich);
        }
    }
}
