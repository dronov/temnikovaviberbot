package me.wobot.temnikova.api.commands.base;

import org.json.JSONObject;

public abstract class AbsRequest {

    public abstract String getMethod();
    public abstract JSONObject toJson();
    public abstract AbsResponse fromResponse(JSONObject answer);
}
