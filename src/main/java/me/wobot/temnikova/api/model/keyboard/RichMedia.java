package me.wobot.temnikova.api.model.keyboard;

import me.wobot.temnikova.api.commands.base.JsonSerializer;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class RichMedia implements JsonSerializer {

    private final String type;
    private final int buttonsGroupColumns;
    private final int buttonsGroupRows;
    private final String bgColor;
    private final List<KeyboardButton> buttons;

    public RichMedia(String type, int buttonsGroupColumns, int buttonsGroupRows, String bgColor, List<KeyboardButton> buttons) {
        this.type = type;
        this.buttonsGroupColumns = buttonsGroupColumns;
        this.buttonsGroupRows = buttonsGroupRows;
        this.bgColor = bgColor;
        this.buttons = buttons;
    }

    @Override
    public void serialize(JSONObject json) {
        json.put("Type", type);
        json.put("ButtonsGroupColumns", buttonsGroupColumns);
        json.put("ButtonsGroupRows", buttonsGroupRows);
        if (bgColor != null) {
            json.put("BgColor", bgColor);
        }
        JSONArray array = new JSONArray();
        for (KeyboardButton button : buttons) {
            JSONObject buttonJson = new JSONObject();
            button.serialize(buttonJson);
            array.put(buttonJson);
        }
        json.put("Buttons", array);
    }

    @Override
    public String toString() {
        return "RichMedia{" +
                "type='" + type + '\'' +
                ", buttonsGroupColumns=" + buttonsGroupColumns +
                ", buttonsGroupRows=" + buttonsGroupRows +
                ", bgColor='" + bgColor + '\'' +
                ", buttons=" + buttons +
                '}';
    }

    public static final class Builder {

        private String type;
        private int buttonsGroupColumns;
        private int buttonsGroupRows;
        private String bgColor;
        private List<KeyboardButton> buttons = new ArrayList<>();

        public Builder type(String type) {
            this.type = type;
            return this;
        }

        public Builder buttonsGroupColumns(int buttonsGroupColumns) {
            this.buttonsGroupColumns = buttonsGroupColumns;
            return this;
        }

        public Builder buttonsGroupRows(int buttonsGroupRows) {
            this.buttonsGroupRows = buttonsGroupRows;
            return this;
        }

        public Builder bgColor(String bgColor) {
            this.bgColor = bgColor;
            return this;
        }

        public Builder addButton(KeyboardButton button) {
            this.buttons.add(button);
            return this;
        }

        public Builder addButtons(List<KeyboardButton> button) {
            this.buttons.addAll(button);
            return this;
        }

        public Builder buttons(List<KeyboardButton> buttons) {
            this.buttons = buttons;
            return this;
        }

        public RichMedia build() {
            if (type == null) {
                type = "rich_media";
            }
            return new RichMedia(type, buttonsGroupColumns, buttonsGroupRows, bgColor, buttons);
        }
    }
}
