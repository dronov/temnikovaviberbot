package me.wobot.temnikova.api.commands;

import me.wobot.temnikova.api.ApiConfig;
import me.wobot.temnikova.api.commands.base.AbsRequest;
import me.wobot.temnikova.api.commands.base.AbsResponse;
import me.wobot.temnikova.api.model.messages.AbsMessage;
import org.json.JSONObject;

public class SendMessage {

    public static class Request extends AbsRequest {

        private final String authToken;
        private final String userId;
        private final AbsMessage message;

        public Request(String userId, AbsMessage message) {
            this(ApiConfig.TOKEN_ACCOUNT, userId, message);
        }

        public Request(String authToken, String userId, AbsMessage message) {
            this.authToken = authToken;
            this.userId = userId;
            this.message = message;
        }

        @Override
        public String getMethod() {
            return "send_message";
        }

        @Override
        public JSONObject toJson() {
            JSONObject result = new JSONObject();
            result.put("auth_token", authToken);
            result.put("receiver", userId);
            message.serialize(result);
            return result;
        }

        @Override
        public AbsResponse fromResponse(JSONObject answer) {
            return new Response().fromResponse(answer);
        }
    }

    public static class Response extends AbsResponse {

        private long messageId;
        private int status;
        private String statusMessage;

        @Override
        protected AbsResponse fromResponse(JSONObject response) {
            if (response.has("message_token")) {
                messageId = response.getLong("message_token");
            }
            if (response.has("status")) {
                status = (int) response.get("status");
            }
            if (response.has("status_message")) {
                statusMessage = (String) response.get("status_message");
            }

            return this;
        }

        public long getMessageId() {
            return messageId;
        }

        public int getStatus() {
            return status;
        }

        public String getStatusMessage() {
            return statusMessage;
        }

        @Override
        public String toString() {
            return "Response{" +
                    "messageId='" + messageId + '\'' +
                    ", status=" + status +
                    ", statusMessage='" + statusMessage + '\'' +
                    '}';
        }
    }

}
