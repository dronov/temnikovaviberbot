package me.wobot.temnikova.api.model.messages;

import me.wobot.temnikova.api.model.keyboard.Keyboard;
import org.json.JSONObject;

public class UrlMessage extends AbsMessage {

    private final String url;

    public UrlMessage(String url) {
        this(url, null);
    }

    public UrlMessage(String url, Keyboard keyboard) {
        super(MessageType.URL, keyboard);
        this.url = url;
    }

    @Override
    public void serialize(JSONObject json) {
        super.serialize(json);
        json.put("media", url);
    }

    public String getUrl() {
        return url;
    }

    @Override
    public String toString() {
        return "UrlMessage{" +
                "url='" + url + '\'' +
                '}';
    }
}
