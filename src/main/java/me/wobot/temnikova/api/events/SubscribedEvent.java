package me.wobot.temnikova.api.events;

import me.wobot.temnikova.api.events.base.Event;
import me.wobot.temnikova.api.model.User;
import me.wobot.temnikova.api.utils.Jsons;
import org.json.JSONObject;

public class SubscribedEvent extends Event {

    private final long timestamp;
    private final User user;

    public SubscribedEvent(long timestamp, User user) {
        this.timestamp = timestamp;
        this.user = user;
    }

    public SubscribedEvent(JSONObject json) {
        timestamp = Jsons.safeLong(json, "timestamp");
        user = Jsons.safeUser(json, "user");
    }

    public long getTimestamp() {
        return timestamp;
    }

    public User getUser() {
        return user;
    }

    @Override
    public String toString() {
        return "SubscribedEvent{" +
                "timestamp=" + timestamp +
                ", user=" + user +
                '}';
    }
}
