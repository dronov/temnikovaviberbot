package me.wobot.temnikova.controller;

import me.wobot.temnikova.api.ApiConfig;
import me.wobot.temnikova.api.events.ConversationStartedEvent;
import me.wobot.temnikova.api.events.MessageEvent;
import me.wobot.temnikova.api.model.User;
import me.wobot.temnikova.api.model.messages.TextMessage;
import me.wobot.temnikova.service.ApiService;
import me.wobot.temnikova.service.BotService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/test")
public class TestController {

    @Autowired
    ApiService api;
    @Autowired
    BotService botService;

    @RequestMapping(value = "/send_message", method = RequestMethod.GET)
    public void sendTextMessage() {
//        api.sendTextMessage(ApiConfig.DRONOV_TEST_USER_ID, "Привет!", Keyboards.keyboardMenu());

        botService.onMessageEvent(
                new MessageEvent(0L, 0L, new User(ApiConfig.DRONOV_USER_TEMNIKOVA_ID, "Ilya Dronov", null, null, null, "android", null),
                        new TextMessage("menu_shop"))
        );

    }

    @RequestMapping(value = "/conv_started", method = RequestMethod.GET)
    public void onConvStarted() {
        botService.onReceived(new ConversationStartedEvent(0L, 0L, null,
                new User("ilya.dronov", "Ilya Dronov", null, null, null, "android", null),
                false));
    }

}
