package me.wobot.temnikova.repo;

import me.wobot.temnikova.model.VoteDb;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Transactional
@Repository
public interface VoteRepository extends JpaRepository<VoteDb, Long> {

    long countByVoteIdLike(String pattern);

    long countByVoteIdEquals(String pattern);
}
