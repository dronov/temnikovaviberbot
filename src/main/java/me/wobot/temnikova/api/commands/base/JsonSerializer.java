package me.wobot.temnikova.api.commands.base;

import org.json.JSONObject;

public interface JsonSerializer {

    void serialize(JSONObject json);
}
