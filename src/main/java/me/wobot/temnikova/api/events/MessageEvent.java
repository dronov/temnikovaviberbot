package me.wobot.temnikova.api.events;

import me.wobot.temnikova.api.events.base.Event;
import me.wobot.temnikova.api.model.User;
import me.wobot.temnikova.api.model.messages.AbsMessage;
import me.wobot.temnikova.api.utils.Jsons;
import org.json.JSONObject;

public class MessageEvent extends Event {

    private final long timestamp;
    private final long messageId;
    private final User user;
    private final AbsMessage message;

    public MessageEvent(long timestamp, long messageId, User user, AbsMessage message) {
        this.timestamp = timestamp;
        this.messageId = messageId;
        this.user = user;
        this.message = message;
    }
    public MessageEvent(JSONObject json) {
        timestamp = Jsons.safeLong(json, "timestamp");
        messageId = Jsons.safeLong(json, "message_token");
        user = Jsons.safeUser(json, "sender");
        message = AbsMessage.from(json.getJSONObject("message"));
    }

    public long getTimestamp() {
        return timestamp;
    }

    public long getMessageId() {
        return messageId;
    }

    public User getUser() {
        return user;
    }

    public AbsMessage getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return "MessageEvent{" +
                "timestamp=" + timestamp +
                ", messageId=" + messageId +
                ", user=" + user +
                ", message=" + message +
                '}';
    }
}
