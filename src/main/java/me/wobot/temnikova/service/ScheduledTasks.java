package me.wobot.temnikova.service;

import me.wobot.temnikova.Constants;
import me.wobot.temnikova.api.ApiConfig;
import me.wobot.temnikova.model.ConcertTour;
import me.wobot.temnikova.model.VoteDb;
import me.wobot.temnikova.repo.VoteRepository;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Component
public class ScheduledTasks {

    private final Logger logger = Logger.getLogger(ScheduledTasks.class);

    private static final int FIXED_RATE_MS = 1000 * 60 * 15; // 15 minutes
    private static boolean DEBUG = false;

    @Autowired
    VoteRepository voteDao;
    @Autowired
    BotService botService;

    @Scheduled(fixedRate = FIXED_RATE_MS)
    public void promoPostsNotifier() {
        logger.info("promoPostsNotifierTEMNIKOVA start");

        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("Europe/Moscow"));
        Date now = calendar.getTime();

        List<ConcertTour> filtered = Constants.TOUR_2017_2018
                .stream()
                .filter(concertTour -> concertTour.date.compareTo(now) >= 0)
                .collect(Collectors.toList());

        if (filtered.isEmpty()) {
            return;
        }
        ConcertTour tour = filtered.get(0);

        String id = "city_promo_" + tour.shareAction;

        long counts = voteDao.countByVoteIdLike(id);
        logger.info("promoPostsNotifierTEMNIKOVA, countByVoteIdLike = " + counts + ", tour = " + tour.city);
        if (counts == 0) {

            long hours = TimeUnit.MILLISECONDS.toHours(tour.date.getTime() - now.getTime());
            logger.info("promoPostsNotifierTEMNIKOVA, counts = 1, hours between = " + hours);
            if ((hours <= 24 && hours > 2) || DEBUG) {
                // START VOITING
                voteDao.save(new VoteDb(ApiConfig.DRONOV_TEST_USER_ID, id));
                botService.broadcastStartPromoTour(tour);

            }
        } else if (counts == 1) {

            long hours = TimeUnit.MILLISECONDS.toHours(tour.date.getTime() - now.getTime());
            logger.info("promoPostsNotifierTEMNIKOVA, counts = 2, hours between = " + hours);
            if ((hours <= 2 && hours > 0) || DEBUG) {
                // STOP VOITING
                voteDao.save(new VoteDb(ApiConfig.DRONOV_TEST_USER_ID, id));
                botService.broadcastEndPromoTour(tour);

            }
        }
    }
}
