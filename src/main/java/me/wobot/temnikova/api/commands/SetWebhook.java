package me.wobot.temnikova.api.commands;

import me.wobot.temnikova.api.ApiConfig;
import me.wobot.temnikova.api.commands.base.AbsRequest;
import me.wobot.temnikova.api.commands.base.AbsResponse;
import org.json.JSONObject;

public class SetWebhook {

    public static class Request extends AbsRequest {

        private final String authToken;
        private final String url;

        public Request(String url) {
            this(ApiConfig.TOKEN_ACCOUNT, url);
        }

        public Request(String authToken, String url) {
            this.authToken = authToken;
            this.url = url;
        }

        @Override
        public String getMethod() {
            return "set_webhook";
        }

        @Override
        public JSONObject toJson() {
            JSONObject json = new JSONObject();
            json.put("auth_token", authToken);
            json.put("url", url);
            return json;
        }

        @Override
        public AbsResponse fromResponse(JSONObject answer) {
            return new Response().fromResponse(answer);
        }

    }

    public static class Response extends AbsResponse {

        private int status;
        private String statusMessage;

        Response() {
        }

        @Override
        protected Response fromResponse(JSONObject response) {
            if (response.has("status")) {
                status = (int) response.get("status");
            }
            if (response.has("status_message")) {
                statusMessage = (String) response.get("status_message");
            }
            return this;
        }

        public int getStatus() {
            return status;
        }

        public String getStatusMessage() {
            return statusMessage;
        }

        @Override
        public String toString() {
            return "Response{" +
                    "status=" + status +
                    ", statusMessage='" + statusMessage + '\'' +
                    '}';
        }
    }

}
