package me.wobot.temnikova.api.events.base;

import me.wobot.temnikova.api.events.ConversationStartedEvent;
import me.wobot.temnikova.api.events.MessageEvent;
import me.wobot.temnikova.api.events.SubscribedEvent;
import me.wobot.temnikova.api.events.UnsubscribedEvent;
import org.json.JSONObject;

public abstract class Event {

    public static Event from(JSONObject object) {
        if (!object.has("event")) {
            throw new IllegalArgumentException("JSONObject don't have parameter event"); // TODO: only for debug
        }

        String event = object.getString("event");
        switch (event) {
            case "subscribed": return new SubscribedEvent(object);
            case "unsubscribed": return new UnsubscribedEvent(object);
            case "conversation_started": return new ConversationStartedEvent(object);
            case "message": return new MessageEvent(object);
            default: return null;
        }
    }
}
