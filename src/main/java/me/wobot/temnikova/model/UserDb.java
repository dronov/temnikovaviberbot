package me.wobot.temnikova.model;

import lombok.Data;
import me.wobot.temnikova.api.model.User;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "UserDb")
public class UserDb implements Serializable {

    public static int STATUS_UNSUBSCRIBED = 0;
    public static int STATUS_SUBSCRIBED = 1;

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;

    @Column(name = "user_id", nullable = false)
    String userId;

    @Column(length = 10000, nullable = true)
    String name;

    @Column(length = 10000, nullable = true)
    String avatarUrl;

    String country;
    String language;
    @Column(name = "deviceOs", nullable = true)
    String primaryDeviceOs;

    @ColumnDefault("0")
    @Column(name = "status")
    int status;

    @ColumnDefault("0")
    @Column(name = "promo_tour_voted")
    int promoTourVoted;

    public boolean isSubscribed() {
        return status == STATUS_SUBSCRIBED;
    }

    public boolean isIos() {
        if (primaryDeviceOs == null) {
            return true;
        }
        if (primaryDeviceOs.toLowerCase().contains("android")) {
            return false;
        }
        return true;
    }

    public UserDb() {
    }

    public UserDb(String userId, String name, String avatarUrl, String country, String language, String primaryDeviceOs, int status) {
        this.userId = userId;
        this.name = name;
        this.avatarUrl = avatarUrl;
        this.country = country;
        this.language = language;
        this.primaryDeviceOs = primaryDeviceOs;
        this.status = status;
    }

    public static UserDb from(User user) {
        return from(user, STATUS_UNSUBSCRIBED);
    }

    public static UserDb from(User user, int status) {
        return new UserDb(user.getId(), user.getName(), user.getAvatarUrl(), user.getCountry(), user.getLanguage(),
                user.getPrimaryDeviceOs(), status);
    }
}
