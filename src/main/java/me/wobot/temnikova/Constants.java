package me.wobot.temnikova;

import me.wobot.temnikova.model.ConcertTour;
import me.wobot.temnikova.model.PromoPost;
import me.wobot.temnikova.model.ShopItem;
import me.wobot.temnikova.model.Track;

import java.util.ArrayList;
import java.util.List;

public class Constants {

    public static final String IOS_TEMNIKOVA_1 = "https://itunes.apple.com/ru/album/temnikova-i/id1147199446";
    public static final String ANDROID_TEMNIKOVA_1 = "https://play.google.com/store/music/album/%D0%95%D0%BB%D0%B5%D0%BD%D0%B0_%D0%A2%D0%B5%D0%BC%D0%BD%D0%B8%D0%BA%D0%BE%D0%B2%D0%B0_TEMNIKOVA_I?id=B3j6zcyg4y2zxwtrdlcnpbtxkhu";

    public static final String IOS_TEMNIKOVA_2 = "https://itunes.apple.com/ru/album/temnikova-ii/id1272097429";
    public static final String ANDROID_TEMNIKOVA_2 = "https://play.google.com/store/music/album/%D0%95%D0%BB%D0%B5%D0%BD%D0%B0_%D0%A2%D0%B5%D0%BC%D0%BD%D0%B8%D0%BA%D0%BE%D0%B2%D0%B0_TEMNIKOVA_II?id=Bl4e74giqz4qko42oynfhotx4za";

    public static final String IOS_TEMNIKOVA_3 = "https://apple.co/2GEplhg";
    public static final String ANDROID_TEMNIKOVA_3 = "https://bit.ly/2HwjuKz";

    public static final List<Track> VOTE_TEMNIKOVA_2 = new ArrayList<Track>() {{
       add(new Track("http://image.ibb.co/dtVKJ6/2_temnikova2_kazalsastrannim.jpg",
               "https://www.youtube.com/watch?v=wBmXhVELbpY", "Казался странным",
               "vote_temnikova_2_kazalsya_strannum",
               null, null));

        add(new Track("http://image.ibb.co/jgVx5m/2_temnikova2_vdoh.jpg",
                "https://www.youtube.com/watch?v=1TC_Hd5e8is", "Вдох",
                "vote_temnikova_2_vxod",
                null, null));
    }};

    public static final List<Track> VOTE_TEMNIKOVA_1 = new ArrayList<Track>() {{
        add(new Track("http://image.ibb.co/gYUPkm/3_temnikova1_dvijenia.jpg",
                "https://www.youtube.com/watch?v=wAzt0jdZHoc", "Движения",
                "vote_temnikova_1_dvigeniya",
                null, null));

        add(new Track("http://image.ibb.co/kJ2c5m/3_temnikova1_teplo.jpg",
                "https://www.youtube.com/watch?v=_yejiN8Sl9s", "Тепло",
                "vote_temnikova_1_teplo",
                null, null));
    }};

    public static final List<Track> TEMNIKOVA_1 = new ArrayList<Track>() {{
        add(new Track("http://image.ibb.co/ejq8BR/music_temnikova1_1_impulsi.jpg",
                "https://www.youtube.com/watch?v=-T5G2j9Og8c", "Импульсы", "temnikova_1_impulsu",
                "https://itunes.apple.com/ru/album/%D0%B8%D0%BC%D0%BF%D1%83%D0%BB%D1%8C%D1%81%D1%8B-%D0%B3%D0%BE%D1%80%D0%BE%D0%B4%D0%B0/id1147199446?i=1147199511",
                "https://play.google.com/store/music/album/%D0%95%D0%BB%D0%B5%D0%BD%D0%B0_%D0%A2%D0%B5%D0%BC%D0%BD%D0%B8%D0%BA%D0%BE%D0%B2%D0%B0_TEMNIKOVA_I?id=B3j6zcyg4y2zxwtrdlcnpbtxkhu"));

        add(new Track("http://image.ibb.co/fknYd6/music_temnikova1_2_blije.jpg",
                "https://www.youtube.com/watch?v=DzL2u84SDCc", "Ближе", "temnikova_1_blije",
                "https://itunes.apple.com/ru/album/%D0%B1%D0%BB%D0%B8%D0%B6%D0%B5/id1147199446?i=1147199796",
                "https://play.google.com/store/music/album/%D0%95%D0%BB%D0%B5%D0%BD%D0%B0_%D0%A2%D0%B5%D0%BC%D0%BD%D0%B8%D0%BA%D0%BE%D0%B2%D0%B0_TEMNIKOVA_I?id=B3j6zcyg4y2zxwtrdlcnpbtxkhu"));

        add(new Track("http://image.ibb.co/dvjZJ6/music_temnikova1_3_dvijenia_1.jpg",
                "https://www.youtube.com/watch?v=wAzt0jdZHoc", "Движения", "temnikova_1_dvigeniya",
                "https://itunes.apple.com/ru/album/%D0%B4%D0%B2%D0%B8%D0%B6%D0%B5%D0%BD%D0%B8%D1%8F/id1147199446?i=1147199783",
                "https://play.google.com/store/music/album/%D0%95%D0%BB%D0%B5%D0%BD%D0%B0_%D0%A2%D0%B5%D0%BC%D0%BD%D0%B8%D0%BA%D0%BE%D0%B2%D0%B0_TEMNIKOVA_I?id=B3j6zcyg4y2zxwtrdlcnpbtxkhu"));

        add(new Track("http://image.ibb.co/eXfVQm/music_temnikova1_4_ponizam.jpg",
                "https://www.youtube.com/watch?v=Hbu9g6m6YiI", "По низам", "temnikova_1_po_nizam",
                "https://itunes.apple.com/ru/album/%D0%BF%D0%BE-%D0%BD%D0%B8%D0%B7%D0%B0%D0%BC/id1147199446?i=1147199524",
                "https://play.google.com/store/music/album/%D0%95%D0%BB%D0%B5%D0%BD%D0%B0_%D0%A2%D0%B5%D0%BC%D0%BD%D0%B8%D0%BA%D0%BE%D0%B2%D0%B0_TEMNIKOVA_I?id=B3j6zcyg4y2zxwtrdlcnpbtxkhu"));

        add(new Track("http://image.ibb.co/fZAVQm/music_temnikova1_5_schastie.jpg",
                "https://www.youtube.com/watch?v=lxPGewpieks", "Счастье", "temnikova_1_shastie",
                "https://itunes.apple.com/ru/album/%D1%81%D1%87%D0%B0%D1%81%D1%82%D1%8C%D0%B5/id1147199446?i=1147199778",
                "https://play.google.com/store/music/album/%D0%95%D0%BB%D0%B5%D0%BD%D0%B0_%D0%A2%D0%B5%D0%BC%D0%BD%D0%B8%D0%BA%D0%BE%D0%B2%D0%B0_TEMNIKOVA_I?id=B3j6zcyg4y2zxwtrdlcnpbtxkhu"
                ));

        add(new Track("http://image.ibb.co/c73Td6/music_temnikova1_6_teplo_1.jpg",
                "https://www.youtube.com/watch?v=_yejiN8Sl9s", "Тепло", "temnikova_1_teplo",
                "https://itunes.apple.com/ru/album/%D1%82%D0%B5%D0%BF%D0%BB%D0%BE/id1147199446?i=1147199521",
                "https://play.google.com/store/music/album/%D0%95%D0%BB%D0%B5%D0%BD%D0%B0_%D0%A2%D0%B5%D0%BC%D0%BD%D0%B8%D0%BA%D0%BE%D0%B2%D0%B0_TEMNIKOVA_I?id=B3j6zcyg4y2zxwtrdlcnpbtxkhu"
                ));
    }};

    public static final List<Track> TEMNIKOVA_2 = new ArrayList<Track>() {{
        add(new Track("http://image.ibb.co/dQ8By6/music_temnikova2_1_teplo_1.jpg",
                "https://www.youtube.com/watch?v=1TC_Hd5e8is", "Вдох", "temnikova_2_vxod",
                "https://itunes.apple.com/ru/album/%D0%B2%D0%B4%D0%BE%D1%85/id1272097429?i=1272097961",
                "https://play.google.com/store/music/album/%D0%95%D0%BB%D0%B5%D0%BD%D0%B0_%D0%A2%D0%B5%D0%BC%D0%BD%D0%B8%D0%BA%D0%BE%D0%B2%D0%B0_TEMNIKOVA_II?id=Bl4e74giqz4qko42oynfhotx4za"
                ));

        add(new Track("http://image.ibb.co/fXwFrR/music_temnikova2_2_podsipal.jpg",
                "https://www.youtube.com/watch?v=c5nHXhQYnbU", "Подсыпал", "temnikova_2_podsupal",
                "https://itunes.apple.com/ru/album/%D0%BF%D0%BE%D0%B4%D1%81%D1%8B%D0%BF%D0%B0%D0%BB/id1272097429?i=1272098382",
                "https://play.google.com/store/music/album/%D0%95%D0%BB%D0%B5%D0%BD%D0%B0_%D0%A2%D0%B5%D0%BC%D0%BD%D0%B8%D0%BA%D0%BE%D0%B2%D0%B0_TEMNIKOVA_II?id=Bl4e74giqz4qko42oynfhotx4za"
                ));

        add(new Track("http://image.ibb.co/mvJuJ6/music_temnikova2_3_kazalsastrannim_1.jpg",
                "https://www.youtube.com/watch?v=wBmXhVELbpY", "Казался странным", "temnikova_2_kazalsya_strannum",
                "https://itunes.apple.com/ru/album/%D0%BA%D0%B0%D0%B7%D0%B0%D0%BB%D1%81%D1%8F-%D1%81%D1%82%D1%80%D0%B0%D0%BD%D0%BD%D1%8B%D0%BC/id1272097429?i=1272098289",
                "https://play.google.com/store/music/album/%D0%95%D0%BB%D0%B5%D0%BD%D0%B0_%D0%A2%D0%B5%D0%BC%D0%BD%D0%B8%D0%BA%D0%BE%D0%B2%D0%B0_TEMNIKOVA_II?id=Bl4e74giqz4qko42oynfhotx4za"
                ));

        add(new Track("http://image.ibb.co/e9NYd6/music_temnikova2_4_neveruya.jpg",
                "https://www.youtube.com/watch?v=Kxn6ErHzVFQ", "Не верю я", "temnikova_2_ne_veruy_ya",
                "https://itunes.apple.com/ru/album/%D0%BD%D0%B5-%D0%B2%D0%B5%D1%80%D1%8E-%D1%8F/id1272097429?i=1272098290",
                "https://play.google.com/store/music/album/%D0%95%D0%BB%D0%B5%D0%BD%D0%B0_%D0%A2%D0%B5%D0%BC%D0%BD%D0%B8%D0%BA%D0%BE%D0%B2%D0%B0_TEMNIKOVA_II?id=Bl4e74giqz4qko42oynfhotx4za"
                ));

        add(new Track("http://image.ibb.co/jovVQm/music_temnikova2_5_soberimena.jpg",
                "https://www.youtube.com/watch?v=TErnEu-_XAo", "Собери меня", "temnikova_2_soberi_menya",
                "https://itunes.apple.com/ru/album/%D1%81%D0%BE%D0%B1%D0%B5%D1%80%D0%B8-%D0%BC%D0%B5%D0%BD%D1%8F/id1272097429?i=1272098384",
                "https://play.google.com/store/music/album/%D0%95%D0%BB%D0%B5%D0%BD%D0%B0_%D0%A2%D0%B5%D0%BC%D0%BD%D0%B8%D0%BA%D0%BE%D0%B2%D0%B0_TEMNIKOVA_II?id=Bl4e74giqz4qko42oynfhotx4za"
                ));

        add(new Track("http://image.ibb.co/gG0my6/music_temnikova2_6_vnezapno.jpg",
                "https://www.youtube.com/watch?v=s2N4wGLwrkc", "Срываюсь внезапно", "temnikova_2_srivays_vnezapno",
                "https://itunes.apple.com/ru/album/%D1%81%D1%80%D1%8B%D0%B2%D0%B0%D1%8E%D1%81%D1%8C-%D0%B2%D0%BD%D0%B5%D0%B7%D0%B0%D0%BF%D0%BD%D0%BE/id1272097429?i=1272098381",
                "https://play.google.com/store/music/album/%D0%95%D0%BB%D0%B5%D0%BD%D0%B0_%D0%A2%D0%B5%D0%BC%D0%BD%D0%B8%D0%BA%D0%BE%D0%B2%D0%B0_TEMNIKOVA_II?id=Bl4e74giqz4qko42oynfhotx4za"
                ));

//        add(new Track("https://vk.com/videos243556640?section=album_55626762&z=video243556640_456239287%2Fpl_243556640_55626762",
//                "https://www.youtube.com/watch?v=_yejiN8Sl9s",
//                "Фиолетовый", "temnikova_2_fioletoviy"));
    }};

    public static final List<Track> TEMNIKOVA_3 = new ArrayList<Track>() {{
        add(new Track("https://i.ibb.co/g7Qy18K/temnikova-ne-modniy.jpg",
                "https://www.youtube.com/watch?v=V7hd5JF2ncs&list=OLAK5uy_mBwhF5c6mb3Gsy9M8HVOxjByaZbdX9D3M&index=2", "Не модные", "temnikova_3_ne_modniy",
                "https://itunes.apple.com/ru/album/%D0%BD%D0%B5-%D0%BC%D0%BE%D0%B4%D0%BD%D1%8B%D0%B5/1367414565?i=1367414898",
                "https://play.google.com/store/music/album/%D0%95%D0%BB%D0%B5%D0%BD%D0%B0_%D0%A2%D0%B5%D0%BC%D0%BD%D0%B8%D0%BA%D0%BE%D0%B2%D0%B0_TEMNIKOVA_III_%D0%9D%D0%B5_%D0%BC%D0%BE%D0%B4%D0%BD%D1%8B%D0%B5?id=Bwhgkgbee3i27vap72j6eppawqa"));

        add(new Track("https://i.ibb.co/cx69H3y/image.jpg",
                "https://www.youtube.com/watch?v=T3BrBaP0hvA&index=3&list=OLAK5uy_mBwhF5c6mb3Gsy9M8HVOxjByaZbdX9D3M", "Не сдерживай меня", "temnikova_3_ne_sderjivay_menya",
                "https://itunes.apple.com/ru/album/%D0%BD%D0%B5-%D1%81%D0%B4%D0%B5%D1%80%D0%B6%D0%B8%D0%B2%D0%B0%D0%B9-%D0%BC%D0%B5%D0%BD%D1%8F/1367414565?i=1367414907",
                "https://play.google.com/store/music/album/%D0%95%D0%BB%D0%B5%D0%BD%D0%B0_%D0%A2%D0%B5%D0%BC%D0%BD%D0%B8%D0%BA%D0%BE%D0%B2%D0%B0_TEMNIKOVA_III_%D0%9D%D0%B5_%D0%BC%D0%BE%D0%B4%D0%BD%D1%8B%D0%B5?id=Bwhgkgbee3i27vap72j6eppawqa"));

        add(new Track("https://i.ibb.co/hfs7P3n/image.jpg",
                "https://www.youtube.com/watch?v=AIuQsserPXg&index=8&list=OLAK5uy_mBwhF5c6mb3Gsy9M8HVOxjByaZbdX9D3M", "Медленно", "temnikova_3_medlenno",
                "https://itunes.apple.com/ru/album/%D0%BC%D0%B5%D0%B4%D0%BB%D0%B5%D0%BD%D0%BD%D0%BE/1367414565?i=1367414918",
                "https://www.youtube.com/redirect?event=video_description&v=AIuQsserPXg&redir_token=IzBFI5Usi4CR3SjlKZ96oHRlZx98MTU0NTY5MjAxNEAxNTQ1NjA1NjE0&q=http%3A%2F%2Fbit.ly%2F2HFa8ZZ"));

        add(new Track("https://i.ibb.co/ftyxVw8/Temnikova-cover.jpg",
                "https://www.youtube.com/watch?v=V3-Lg679h3c&list=OLAK5uy_mBwhF5c6mb3Gsy9M8HVOxjByaZbdX9D3M&index=5", "Нетрезвыми", "temnikova_3_netrezvimy",
                "https://itunes.apple.com/ru/album/%D0%BD%D0%B5%D1%82%D1%80%D0%B5%D0%B7%D0%B2%D1%8B%D0%BC%D0%B8/1367414565?i=1367414909",
                "https://itunes.apple.com/ru/album/%D0%BD%D0%B5%D1%82%D1%80%D0%B5%D0%B7%D0%B2%D1%8B%D0%BC%D0%B8/1367414565?i=1367414909"));

        add(new Track("https://i.ibb.co/ftyxVw8/Temnikova-cover.jpg",
                "https://www.youtube.com/watch?v=LeK6gXOFWYg&index=7&list=OLAK5uy_mBwhF5c6mb3Gsy9M8HVOxjByaZbdX9D3M", "Что-то не так", "temnikova_3_chto_to_ne_tak",
                "https://itunes.apple.com/ru/album/%D1%87%D1%82%D0%BE-%D1%82%D0%BE-%D0%BD%D0%B5-%D1%82%D0%B0%D0%BA/1367414565?i=1367414917",
                "https://itunes.apple.com/ru/album/%D1%87%D1%82%D0%BE-%D1%82%D0%BE-%D0%BD%D0%B5-%D1%82%D0%B0%D0%BA/1367414565?i=1367414917"
        ));

        add(new Track("https://i.ibb.co/VJXNmXH/image.jpg",
                "https://www.youtube.com/watch?v=ZRnUy2RbsyM&index=12&list=OLAK5uy_mBwhF5c6mb3Gsy9M8HVOxjByaZbdX9D3M", "Мне нормально", "temnikova_3_mne_normalno",
                "https://itunes.apple.com/ru/album/%D0%BC%D0%BD%D0%B5-%D0%BD%D0%BE%D1%80%D0%BC%D0%B0%D0%BB%D1%8C%D0%BD%D0%BE/1367414565?i=1367415524",
                "https://play.google.com/store/music/album/%D0%95%D0%BB%D0%B5%D0%BD%D0%B0_%D0%A2%D0%B5%D0%BC%D0%BD%D0%B8%D0%BA%D0%BE%D0%B2%D0%B0_%D0%9C%D0%BD%D0%B5_%D0%BD%D0%BE%D1%80%D0%BC%D0%B0%D0%BB%D1%8C%D0%BD%D0%BE?id=Bivypdz47ezlgqte56hyeezxojq"
        ));

    }};

    public static final List<Track> TEMNIKOVA_NEW_CLIPS = new ArrayList<Track>() {{

        add(new Track("https://i.ibb.co/FDCLR4n/image.jpg",
                "https://youtu.be/wBmXhVELbpY", "Казался странным", "temnikova_2_kazalsya_strannum",
                null,
                null
        ));

        add(new Track("https://i.ibb.co/yFKTGdN/image.jpg",
                "https://youtu.be/P-lL_N1Dprg", "Фиолетовый", "temnikova_2_fioletoviy",
                null,
                null
        ));

        add(new Track("https://i.ibb.co/8cTc1FV/image.jpg",
                "https://www.youtube.com/watch?v=c5nHXhQYnbU", "Подсыпал (LIVE-клип)", "temnikova_2_podsupal",
                null,
                null
        ));

        add(new Track("https://i.ibb.co/cx69H3y/image.jpg",
                "https://youtu.be/T3BrBaP0hvA", "Не сдерживай меня", "temnikova_3_ne_sderjivay_menya",
                null,
                null
        ));

        add(new Track("https://i.ibb.co/DMd8NGz/image.jpg",
                "https://youtu.be/Pl8u2kY_3IQ", "Не модные", "temnikova_3_ne_modniy",
                null,
                null
        ));

        add(new Track("https://i.ibb.co/CzxpmBP/image.jpg",
                "https://youtu.be/99CtPCu_BN0", "Не модные (вертикальный клип)", "temnikova_3_ne_modniy",
                null,
                null
        ));
    }};

    public static final List<ConcertTour> TOUR_2017_2018 = new ArrayList<ConcertTour>() {{
        add(new ConcertTour("Новосибирск", "25.10.2017 15:00", "http://image.ibb.co/fZgvQm/Citi_1.jpg",
                "https://nsk.ponominalu.ru/event/temnikova-elena-vladimirovna?_ga=2.29129020.1812021227.1508096047-1837489812.1508008219", "share_novosibirsk"));

        add(new ConcertTour("Иркутск", "26.10.2017 16:00", "http://image.ibb.co/ij50rR/Citi_2.jpg",
                "https://irk.ponominalu.ru/event/temnikova-elena-vladimirovna?_ga=2.29129020.1812021227.1508096047-1837489812.1508008219", "share_irkuts"));

        add(new ConcertTour("Томск", "28.10.2017 15:00", "http://image.ibb.co/n8yLrR/Citi_3.jpg",
                "https://tomsk.ponominalu.ru/event/temnikova-elena-vladimirovna?_ga=2.135691569.1812021227.1508096047-1837489812.1508008219", "share_tomsk"));

        add(new ConcertTour("Казань", "31.10.2017 19:00", "http://image.ibb.co/gVrE1R/Citi_4_1.jpg",
                "https://kzn.ponominalu.ru/event/temnikova-elena-vladimirovna?_ga=2.135691569.1812021227.1508096047-1837489812.1508008219", "share_kazan"));

        add(new ConcertTour("Самара", "02.11.2017 18:00", "http://image.ibb.co/c0xry6/Citi_5.jpg",
                "https://samara.ponominalu.ru/event/temnikova-elena-vladimirovna?_ga=2.135691569.1812021227.1508096047-1837489812.1508008219", "share_samara"));

        add(new ConcertTour("Саратов", "03.11.2017 18:00", "http://image.ibb.co/ewTaQm/Citi_6.jpg",
                "https://sarat.ponominalu.ru/event/temnikova-elena-vladimirovna?_ga=2.135691569.1812021227.1508096047-1837489812.1508008219", "share_saratov"));

        add(new ConcertTour("Нижний Новгород", "10.11.2017 19:00", "http://image.ibb.co/ksFpkm/Citi_7.jpg",
                "https://nnov.ponominalu.ru/event/temnikova-elena-vladimirovna?_ga=2.135691569.1812021227.1508096047-1837489812.1508008219", "share_nigniy_novgorod"));

        add(new ConcertTour("Пермь", "11.11.2017 17:00", "http://image.ibb.co/fs5YBR/Citi_8.jpg",
                "https://perm.ponominalu.ru/event/temnikova-elena-vladimirovna?_ga=2.135691569.1812021227.1508096047-1837489812.1508008219", "share_perm"));

        add(new ConcertTour("Южно-Сахалинск", "15.11.2017 11:00", "http://image.ibb.co/dyuh5m/Citi_9.jpg",
                "https://yusah.ponominalu.ru/event/temnikova-elena-vladimirovna?_ga=2.26637629.1812021227.1508096047-1837489812.1508008219", "share_ygno_sakhalinsk"));

        add(new ConcertTour("Хабаровск", "16.11.2017 12:00", "http://image.ibb.co/koc25m/Citi_10.jpg",
                "https://habar.ponominalu.ru/event/temnikova-elena-vladimirovna?_ga=2.26637629.1812021227.1508096047-1837489812.1508008219", "share_xabarovsk"));

        add(new ConcertTour("Владивосток", "17.11.2017 12:00", "http://image.ibb.co/euDLrR/Citi_11.jpg",
                "https://vl.ponominalu.ru/event/temnikova-elena-vladimirovna?_ga=2.26637629.1812021227.1508096047-1837489812.1508008219", "share_vladivostok"));

        add(new ConcertTour("Минск", "23.11.2017 20:00", "http://image.ibb.co/fp0N5m/Citi_12.jpg",
                "https://ponominalu.ru/", "share_minsk"));

        add(new ConcertTour("Воронеж", "25.11.2017 19:00", "http://image.ibb.co/gFeFQm/Citi_13.jpg",
                "https://vrn.ponominalu.ru/event/temnikova-elena-vladimirovna?_ga=2.26637629.1812021227.1508096047-1837489812.1508008219", "share_voroneg"));

        add(new ConcertTour("Рязань", "26.11.2017 20:00", "http://image.ibb.co/dRWJd6/Citi_14.jpg",
                "https://rzn.ponominalu.ru/event/temnikova-elena-vladimirovna?_ga=2.26637629.1812021227.1508096047-1837489812.1508008219", "share_ryazan"));

        add(new ConcertTour("Тверь", "30.11.2017 19:00", "http://image.ibb.co/eWLBy6/Citi_15.jpg",
                "https://tver.ponominalu.ru/event/temnikova-elena-vladimirovna?_ga=2.61823148.1812021227.1508096047-1837489812.1508008219", "share_tver"));

        add(new ConcertTour("Смоленск", "01.12.2017 19:00", "http://image.ibb.co/k9Jyd6/Citi_16.jpg",
                "https://smol.ponominalu.ru/event/temnikova-elena-vladimirovna?_ga=2.61823148.1812021227.1508096047-1837489812.1508008219", "share_smolensk"));

        add(new ConcertTour("Иваново", "04.12.2017 19:00", "http://image.ibb.co/n7MJd6/Citi_17.jpg",
                "https://ivanovo.ponominalu.ru/event/temnikova-elena-vladimirovna?_ga=2.61823148.1812021227.1508096047-1837489812.1508008219", "share_ivanovo"));

        add(new ConcertTour("Ярославль", "05.12.2017 19:00", "http://image.ibb.co/g1vBy6/Citi_18.jpg",
                "https://jar.ponominalu.ru/event/temnikova-elena-vladimirovna?_ga=2.61823148.1812021227.1508096047-1837489812.1508008219", "share_yaroslavl"));

        add(new ConcertTour("Ростов-на-Дону", "07.12.2017 19:00", "http://image.ibb.co/d3b6WR/Citi_19.jpg",
                "https://rnd.ponominalu.ru/event/temnikova-elena-vladimirovna?_ga=2.61823148.1812021227.1508096047-1837489812.1508008219", "share_rostov-na-dony"));

        add(new ConcertTour("Краснодар", "08.12.2017 19:00", "http://image.ibb.co/coq0rR/Citi_20.jpg",
                "https://krd.ponominalu.ru/event/temnikova-elena-vladimirovna?_ga=2.61823148.1812021227.1508096047-1837489812.1508008219", "share_krasnodar"));

        add(new ConcertTour("Москва", "14.01.2018 19:00", "http://image.ibb.co/mpV0rR/Citi_21.jpg",
                "https://ponominalu.ru/event/temnikova-elena-vladimirovna/", "share_moskva"));
    }};

    public static final List<PromoPost> PROMO_POSTS = new ArrayList<PromoPost>() {{
        add(new PromoPost("http://image.ibb.co/gGiE8G/act_1.jpg", "ЕЩЕ ПЯТЬ КЭСОВ", "Расстрелять экстра 5К T$ баксов TEMNIKOVABANK",
                "promo_vote_eshe_pyat_kesov"));

        add(new PromoPost("http://image.ibb.co/g9rCvw/act_2.jpg", "ПОДКИНУТЬ ЧУДЕС", "Раскидать чудеса по площадке",
                "promo_vote_podkinyt_chydes"));

        add(new PromoPost("http://image.ibb.co/b9hwNb/act_3.jpg", "В РАССЫПНУЮ", "Раскидать +5 снежков со сцены для доступа в гримерку",
                "promo_vote_v_rassypnyy"));

        add(new PromoPost("http://image.ibb.co/g3gqhb/act_4.jpg", "АВТОПРОХОДКА", "Кинуть в зал футболку для прохода в гримерку",
                "promo_vote_avtoprokhodka"));
    }};


    public static final List<ShopItem> SHOP_ITEMS = new ArrayList<ShopItem>() {{
//        add(new ShopItem("http://image.ibb.co/fKcKaw/banner1.jpg", "Магазин одежды и аксессуров",
//                "https://goo.gl/sMVXN1"));

        add(new ShopItem("https://i.ibb.co/T027RGN/temnikova-longslivi.jpg", "Лонгсливы",
                "https://temnikova.shop/catalog/longslivy/"));

        add(new ShopItem("https://i.ibb.co/89QN2ZN/1.jpg", "Футболки",
                "https://temnikova.shop/catalog/futbolki/"));

        add(new ShopItem("https://i.ibb.co/jD0d83M/temnikova-switshoti.jpg", "Свитшоты",
                "https://temnikova.shop/catalog/svitshoty-/"));

        add(new ShopItem("https://i.ibb.co/S6KLX4B/temnikova-dlya-golovi.jpg", "Для головы",
                "https://temnikova.shop/catalog/dlya-golovy/"));

        add(new ShopItem("https://i.ibb.co/X3sK9zQ/image.png", "Штаны",
                "https://temnikova.shop/catalog/sportivnye-shtany/"));

        add(new ShopItem("https://i.ibb.co/HFzjNNh/image.jpg", "Аксессуары",
                "https://temnikova.shop/catalog/aksessuary/"));
    }};
}
