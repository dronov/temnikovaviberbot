package me.wobot.temnikova.api.model.keyboard;

import lombok.Builder;
import me.wobot.temnikova.api.commands.base.JsonSerializer;
import org.json.JSONObject;

@Builder
public class KeyboardButton implements JsonSerializer {

    private final int width; // 1-6
    private final int height; // 1-2

    private final String bgColor;
    private final boolean silent;
    private final String bgMediaType;
    private final String bgMedia;
    private final boolean bgLoop;

    private final String actionType;
    private final String actionBody;

    private final String image;

    private final String text;
    private final String textVAlign;
    private final String textHAlign;
    private final String textOpacity;
    private final String textSize;

    private final String openUrlType;
    private final String openUrlMediaType;

    private final String internalBrowserActionButton;
    private final String internalBrowserActionPredefinedUrl;

    private KeyboardButton(int width, int height, String bgColor, boolean silent, String bgMediaType, String bgMedia, boolean bgLoop,
                           String actionType, String actionBody, String image, String text, String textVAlign,
                           String textHAlign, String textOpacity, String textSize, String openUrlType,
                           String openUrlMediaType, String internalBrowserActionButton, String internalBrowserActionPredefinedUrl) {
        this.width = width;
        this.height = height;
        this.bgColor = bgColor;
        this.silent = silent;
        this.bgMediaType = bgMediaType;
        this.bgMedia = bgMedia;
        this.bgLoop = bgLoop;
        this.actionType = actionType;
        this.actionBody = actionBody;
        this.image = image;
        this.text = text;
        this.textVAlign = textVAlign;
        this.textHAlign = textHAlign;
        this.textOpacity = textOpacity;
        this.textSize = textSize;
        this.openUrlType = openUrlType;
        this.openUrlMediaType = openUrlMediaType;
        this.internalBrowserActionButton = internalBrowserActionButton;
        this.internalBrowserActionPredefinedUrl = internalBrowserActionPredefinedUrl;
    }

    @Override
    public void serialize(JSONObject json) {
        if (width != 0) {
            json.put("Columns", width);
        }
        if (height != 0) {
            json.put("Rows", height);
        }

        if (bgColor != null) {
            json.put("BgColor", bgColor);
        }

        if (silent) {
            json.put("Silent", true);
        }

        if (bgMediaType != null) {
            json.put("BgMediaType", bgMediaType);
        }

        if (bgMedia != null) {
            json.put("BgMedia", bgMedia);
        }

        if (actionType != null) {
            json.put("ActionType", actionType);
        }

        if (actionBody != null) {
            json.put("ActionBody", actionBody);
        }

        if (image != null) {
            json.put("Image", image);
        }

        if (text != null) {
            json.put("Text", text);
        }

        if (textVAlign != null) {
            json.put("TextVAlign", textVAlign);
        }

        if (textHAlign != null) {
            json.put("TextHAlign", textHAlign);
        }

        if (textOpacity != null) {
            json.put("TextOpacity", textOpacity);
        }

        if (textSize != null) {
            json.put("TextSize", textSize);
        }

        if (openUrlType != null) {
            json.put("OpenURLType", openUrlType);
        }

        if (openUrlMediaType != null) {
            json.put("OpenURLMediaType", openUrlMediaType);
        }

        if (internalBrowserActionButton != null) {
            JSONObject internalBrowser = new JSONObject();
            internalBrowser.put("ActionButton", internalBrowserActionButton);
            internalBrowser.put("ActionPredefinedURL", internalBrowserActionPredefinedUrl);

            json.put("InternalBrowser", internalBrowser);
        }

    }

    @Override
    public String toString() {
        return "KeyboardButton{" +
                "width=" + width +
                ", height=" + height +
                ", bgColor='" + bgColor + '\'' +
                ", bgMediaType='" + bgMediaType + '\'' +
                ", bgMedia='" + bgMedia + '\'' +
                ", bgLoop=" + bgLoop +
                ", actionType='" + actionType + '\'' +
                ", actionBody='" + actionBody + '\'' +
                ", image='" + image + '\'' +
                ", text='" + text + '\'' +
                ", textVAlign='" + textVAlign + '\'' +
                ", textHAlign='" + textHAlign + '\'' +
                ", textOpacity='" + textOpacity + '\'' +
                ", textSize='" + textSize + '\'' +
                '}';
    }

    public static final class Builder {

        private int width;
        private int height;
        private String bgColor;
        private boolean silent;
        private String bgMediaType;
        private String bgMedia;
        private boolean bgLoop;
        private String actionType;
        private String actionBody;
        private String image;
        private String text;
        private String textVAlign;
        private String textHAlign;
        private String textOpacity;
        private String textSize;

        private String openUrlType;
        private String openUrlMediaType;

        private String internalBrowserActionButton;
        private String internalBrowserActionPredefinedUrl;

        public Builder width(int width) {
            this.width = width;
            return this;
        }

        public Builder height(int height) {
            this.height = height;
            return this;
        }

        public Builder bgColor(String bgColor) {
            this.bgColor = bgColor;
            return this;
        }

        public Builder silent(boolean silent) {
            this.silent = silent;
            return this;
        }

        public Builder bgMediaType(String bgMediaType) {
            this.bgMediaType = bgMediaType;
            return this;
        }

        public Builder bgMedia(String bgMedia) {
            this.bgMedia = bgMedia;
            return this;
        }

        public Builder bgLoop(boolean bgLoop) {
            this.bgLoop = bgLoop;
            return this;
        }

        public Builder actionType(String actionType) {
            this.actionType = actionType;
            return this;
        }

        public Builder actionBody(String actionBody) {
            this.actionBody = actionBody;
            return this;
        }

        public Builder image(String image) {
            this.image = image;
            return this;
        }

        public Builder text(String text) {
            this.text = text;
            return this;
        }

        public Builder textVAlign(String textVAlign) {
            this.textVAlign = textVAlign;
            return this;
        }

        public Builder textHAlign(String textHAlign) {
            this.textHAlign = textHAlign;
            return this;
        }

        public Builder textOpacity(String textOpacity) {
            this.textOpacity = textOpacity;
            return this;
        }

        public Builder textSize(String textSize) {
            this.textSize = textSize;
            return this;
        }

        public Builder openUrlType(String openUrlType) {
            this.openUrlType = openUrlType;
            return this;
        }

        public Builder openUrlMediaType(String openUrlMediaType) {
            this.openUrlMediaType = openUrlMediaType;
            return this;
        }

        public Builder internalBrowserActionButton(String internalBrowserActionButton) {
            this.internalBrowserActionButton = internalBrowserActionButton;
            return this;
        }

        public Builder internalBrowserActionPredefinedUrl(String internalBrowserActionPredefinedUrl) {
            this.internalBrowserActionPredefinedUrl = internalBrowserActionPredefinedUrl;
            return this;
        }

        public KeyboardButton build() {
            return new KeyboardButton(width, height, bgColor, silent, bgMediaType, bgMedia, bgLoop, actionType, actionBody,
                    image, text, textVAlign, textHAlign, textOpacity, textSize, openUrlType, openUrlMediaType,
                    internalBrowserActionButton, internalBrowserActionPredefinedUrl);
        }
    }
}
