package me.wobot.temnikova.utils;

import java.util.ArrayList;
import java.util.List;

public class Lists {

    public static <T> List<List<T>> splitList(final List<T> ids, int splitBy) {
        List<T> source = new ArrayList<>(ids.size());
        source.addAll(ids);

        List<List<T>> result = new ArrayList<>();
        if (source.size() > 0 && source.size() <= splitBy) {
            result.add(source);
        } else {
            while (source.size() != 0) {
                List<T> subList = source.subList(0, source.size() <= splitBy ? source.size() : splitBy);

                List<T> list = new ArrayList<>(subList.size());
                list.addAll(subList);
                subList.clear();

                result.add(list);
            }
        }
        return result;
    }

}
