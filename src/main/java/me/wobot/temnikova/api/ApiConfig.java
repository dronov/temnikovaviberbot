package me.wobot.temnikova.api;

public class ApiConfig {

    public static final String URL_VIBER_BASE = "https://chatapi.viber.com/pa/";

    public static final String TEMNIKOVA_ACCOUNT = "421bef051c4533ad-e88d39bce29a5857-1f246fbb7e5e0977";
    public static final String TEMNIKOVA_TEST_ACCOUNT = "45a00bfdbfb07e10-115c0ee31d5a0a8c-ef4be655a907d33b";
    public static final String TOKEN_ACCOUNT = TEMNIKOVA_ACCOUNT;

    public static final String DRONOV_TEST_USER_ID = "Y2Fmn1wEdMkReX6Utr0MgQ==";
    public static final String DRONOV_USER_TEMNIKOVA_ID = "D1cGXHTS7Oem2uR8yIAL2A==";
    public static final String DRONOV_ID = DRONOV_USER_TEMNIKOVA_ID;

    public static final String URL_WEBHOOK = "https://aibot.brainex.co/temnikova";

    public static final String NAME_ACCOUNT = "Temnikova Interactive";
    public static final String ICON_ACCOUNT = "http://image.ibb.co/hfUn5m/bot_avatar_1.jpg";
}
