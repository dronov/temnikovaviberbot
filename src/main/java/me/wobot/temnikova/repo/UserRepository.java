package me.wobot.temnikova.repo;

import me.wobot.temnikova.model.UserDb;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
@Repository
public interface UserRepository extends JpaRepository<UserDb, Long> {

    @Query(value = "SELECT u from UserDb u where u.userId = :user_id")
    UserDb selectByUserId(@Param("user_id") String userId);

    @Modifying
    @Query(value = "UPDATE UserDb u SET u.status = :status WHERE u.userId = :userId")
    void changeStatus(@Param("userId") String userId, @Param("status") int status);

    @Modifying
    @Query(value = "UPDATE UserDb u SET u.promoTourVoted = :votedStatus WHERE u.userId = :userId")
    void changePromoVotedStatus(@Param("userId") String userId, @Param("votedStatus") int votedStatus);

    List<UserDb> findAllByStatusEquals(int status);

    List<UserDb> findAllByStatusEqualsAndPromoTourVotedEquals(int status, int promoVotedTour);
}
