package me.wobot.temnikova.service;

import me.wobot.temnikova.api.events.MessageEvent;
import me.wobot.temnikova.api.events.base.Event;
import me.wobot.temnikova.model.ConcertTour;

public interface BotService {
    String onReceived(Event event);
    String onMessageEvent(MessageEvent event);

    void broadcastStartPromoTour(ConcertTour tour);
    void broadcastEndPromoTour(ConcertTour tour);
}
