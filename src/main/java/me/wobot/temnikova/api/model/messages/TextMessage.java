package me.wobot.temnikova.api.model.messages;

import me.wobot.temnikova.api.model.keyboard.Keyboard;
import org.json.JSONObject;

public class TextMessage extends AbsMessage {

    private final String text;

    public TextMessage(String text) {
        super(MessageType.TEXT);
        this.text = text;
    }

    public TextMessage(String text, Keyboard keyboard) {
        super(MessageType.TEXT, keyboard);
        this.text = text;
    }

    TextMessage(JSONObject json) {
        super(MessageType.TEXT);
        this.text = json.getString("text");
    }

    public String getText() {
        return text;
    }

    @Override
    public void serialize(JSONObject json) {
        super.serialize(json);
        json.put("text", text);
    }

    @Override
    public String toString() {
        return "TextMessage{" +
                "text='" + text + '\'' +
                '}';
    }
}
