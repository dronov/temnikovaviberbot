package me.wobot.temnikova.api.events;

import me.wobot.temnikova.api.events.base.Event;
import me.wobot.temnikova.api.utils.Jsons;
import org.json.JSONObject;

public class UnsubscribedEvent extends Event {

    private final long timestamp;
    private final String userId;

    public UnsubscribedEvent(JSONObject json) {
        timestamp = Jsons.safeLong(json, "timestamp");
        userId = Jsons.safeString(json, "user_id");
    }

    public long getTimestamp() {
        return timestamp;
    }

    public String getUserId() {
        return userId;
    }

    @Override
    public String toString() {
        return "UnsubscribedEvent{" +
                "timestamp=" + timestamp +
                ", userId='" + userId + '\'' +
                '}';
    }
}
