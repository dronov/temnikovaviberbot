package me.wobot.temnikova.api.model.keyboard;

import me.wobot.temnikova.api.commands.base.JsonSerializer;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Keyboard implements JsonSerializer {

    private final String type;
    private final boolean defaultHeight;
    private final String bgColor;
    private final List<KeyboardButton> buttons;

    private Keyboard(String type, boolean defaultHeight, String bgColor, List<KeyboardButton> buttons) {
        this.type = type;
        this.defaultHeight = defaultHeight;
        this.bgColor = bgColor;
        this.buttons = buttons;
    }

    public List<KeyboardButton> getButtons() {
        return buttons;
    }

    @Override
    public void serialize(JSONObject json) {
        json.put("Type", type);
        json.put("DefaultHeight", defaultHeight);
        if (bgColor != null) {
            json.put("BgColor", bgColor);
        }

        JSONArray array = new JSONArray();
        for (KeyboardButton button : buttons) {
            JSONObject buttonJson = new JSONObject();
            button.serialize(buttonJson);
            array.put(buttonJson);
        }
        json.put("Buttons", array);
    }

    public static final class Builder {

        private String type;
        private boolean defaultHeight;
        private String bgColor;

        private List<KeyboardButton> buttons = new ArrayList<>();

        public Builder type(String type) {
            this.type = type;
            return this;
        }

        public Builder defaultHeight(boolean defaultHeight) {
            this.defaultHeight = defaultHeight;
            return this;
        }

        public Builder bgColor(String bgColor) {
            this.bgColor = bgColor;
            return this;
        }

        public Builder addButton(KeyboardButton button) {
            this.buttons.add(button);
            return this;
        }

        public Builder addButtons(List<KeyboardButton> button) {
            this.buttons.addAll(button);
            return this;
        }

        public Builder buttons(List<KeyboardButton> buttons) {
            this.buttons = buttons;
            return this;
        }

        public Keyboard build() {
            if (type == null) {
                type = "keyboard";
            }
            return new Keyboard(type, defaultHeight, bgColor, buttons);
        }
    }

    @Override
    public String toString() {
        return "Keyboard{" +
                "type='" + type + '\'' +
                ", defaultHeight=" + defaultHeight +
                ", bgColor='" + bgColor + '\'' +
                ", buttons=" + buttons.size() +
                '}';
    }
}
