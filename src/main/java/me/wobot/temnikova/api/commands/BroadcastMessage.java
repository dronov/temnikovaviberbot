package me.wobot.temnikova.api.commands;

import me.wobot.temnikova.api.ApiConfig;
import me.wobot.temnikova.api.commands.base.AbsRequest;
import me.wobot.temnikova.api.commands.base.AbsResponse;
import me.wobot.temnikova.api.model.messages.AbsMessage;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class BroadcastMessage {

    public static class Request extends AbsRequest {

        private final String authToken;
        private final List<String> userIds;
        private final AbsMessage message;

        public Request(List<String> userIds, AbsMessage message) {
            this(ApiConfig.TOKEN_ACCOUNT, userIds, message);
        }

        public Request(String authToken, List<String> userIds, AbsMessage message) {
            this.authToken = authToken;
            this.userIds = userIds;
            this.message = message;
        }

        @Override
        public String getMethod() {
            return "broadcast_message";
        }

        @Override
        public JSONObject toJson() {
            JSONObject result = new JSONObject();
            result.put("auth_token", authToken);

            JSONArray users = new JSONArray();
            for (String userId : userIds) {
                users.put(userId);
            }

            result.put("broadcast_list", users);
            message.serialize(result);
            return result;
        }

        @Override
        public AbsResponse fromResponse(JSONObject answer) {
            return new BroadcastMessage.Response().fromResponse(answer);
        }
    }

    public static class Response extends AbsResponse {

        private int status;
        private String statusMessage;
        private List<FailReason> failed = new ArrayList<>();

        @Override
        protected AbsResponse fromResponse(JSONObject response) {
            if (response.has("status")) {
                status = (int) response.get("status");
            }
            if (response.has("status_message")) {
                statusMessage = (String) response.get("status_message");
            }
            if (response.has("failed_list")) {
                JSONArray array = response.getJSONArray("failed_list");
                for (int i = 0; i < array.length(); i++) {
                    JSONObject object = array.getJSONObject(i);
                    failed.add(FailReason.fromJson(object));
                }
            }

            return this;
        }

        public int getStatus() {
            return status;
        }

        public String getStatusMessage() {
            return statusMessage;
        }

        public List<FailReason> getFailed() {
            return failed;
        }

        @Override
        public String toString() {
            return "Response{" +
                    "status=" + status +
                    ", statusMessage='" + statusMessage + '\'' +
                    ", failed=" + failed +
                    '}';
        }
    }

}
