package me.wobot.temnikova.controller;

import me.wobot.temnikova.api.ApiConfig;
import me.wobot.temnikova.api.events.base.Event;
import me.wobot.temnikova.service.ApiService;
import me.wobot.temnikova.service.BotService;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.concurrent.TimeUnit;

@Controller
@ResponseBody
public class ApiController {

    private final Logger logger = Logger.getLogger(ApiController.class);

    @Autowired
    BotService botService;
    @Autowired
    ApiService apiService;

    @RequestMapping(value = "/set_webhook", method = RequestMethod.GET)
    public void setWebhook(HttpServletRequest request) {
        logger.debug("/setWebhook, token = " + ApiConfig.TOKEN_ACCOUNT + ", url = " + ApiConfig.URL_WEBHOOK);
        apiService.setWebhook(ApiConfig.TOKEN_ACCOUNT, ApiConfig.URL_WEBHOOK);
    }

    @RequestMapping(value = "/unset_webhook", method = RequestMethod.GET)
    public void unsetWebhook(HttpServletRequest request) {
        logger.debug("/unsetWebhook, token = " + ApiConfig.TOKEN_ACCOUNT);
        apiService.setWebhook(ApiConfig.TOKEN_ACCOUNT, "");
    }

    @RequestMapping(params = {"sig"}, method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<String> onEvent(@RequestParam(value = "sig") String sig,
                                          @RequestBody String body) {
        logger.info("onEvent: body = " + body);
        JSONObject json = new JSONObject(body);

        Event event = Event.from(json);

        long timeInNanos = System.nanoTime();
        String response = botService.onReceived(event);
        timeInNanos = System.nanoTime() - timeInNanos;

        long timeInMs = TimeUnit.MILLISECONDS.convert(timeInNanos, TimeUnit.NANOSECONDS);
        logger.info("onReceived execution method time in ms = " + timeInMs);

        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.add("Content-Type", "text/html; charset=utf-8");
        return new ResponseEntity<>(response, responseHeaders, HttpStatus.OK);
    }

}
