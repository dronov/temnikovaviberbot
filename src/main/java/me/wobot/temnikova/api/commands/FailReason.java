package me.wobot.temnikova.api.commands;

import org.json.JSONObject;

public class FailReason {

    public final String receiver;
    public final int status;
    public final String statusMessage;

    public FailReason(String receiver, int status, String statusMessage) {
        this.receiver = receiver;
        this.status = status;
        this.statusMessage = statusMessage;
    }

    public static FailReason fromJson(JSONObject json) {
        return new FailReason(
                json.getString("receiver"),
                json.getInt("status"),
                json.getString("status_message")
        );
    }

    @Override
    public String toString() {
        return "FailReason{" +
                "receiver='" + receiver + '\'' +
                ", status=" + status +
                ", statusMessage='" + statusMessage + '\'' +
                '}';
    }
}
