package me.wobot.temnikova.utils;

import me.wobot.temnikova.api.model.keyboard.Keyboard;
import me.wobot.temnikova.api.model.keyboard.KeyboardButton;

public class Keyboards {


    public static Keyboard keyboardMenu(boolean ios) {
        Keyboard.Builder builder = new Keyboard.Builder();
        builder.defaultHeight(false);

        builder.addButton(buttonMenu("TEMNIKOVA TOUR 2018", "reply", "menu_concert_tour", null));
        builder.addButton(buttonMenu("Видео", "open-url","https://www.youtube.com/playlist?list=PLQ7t2Z77CsoXMTRh1rBZZOKD3lVX6OiV0", null));
//        builder.addButton(buttonMenu("Движуха", "reply", "promo_tour_posts", null));
        builder.addButton(buttonMenu("Музыка", "reply", "menu_music", null));
        builder.addButton(buttonMenu("Магазин", "reply", "menu_shop", null));
        builder.addButton(buttonMenu("Temnikova AR", "open-url",
                ios ? "https://itunes.apple.com/ru/app/temnikova-ar/id1365375550?mt=8" : "https://play.google.com/store/apps/details?id=com.beaversbrothers.artemnikova&hl=en_US", null));
        builder.addButton(buttonMenuSticker());
        return builder.build();
    }

    public static Keyboard keyboardMenuYesNo(boolean ios) {
        Keyboard.Builder builder = new Keyboard.Builder();
        builder.defaultHeight(false);

        builder.addButton(buttonYesNo("Да", "menu_yes"));
        builder.addButton(buttonYesNo("Нет", "menu_no"));

        builder.addButtons(keyboardMenu(ios).getButtons());

        return builder.build();
    }

    public static Keyboard keyboardConcertTour(int nextPage, int prevPage, boolean ios) {
        Keyboard.Builder builder = new Keyboard.Builder();
        builder.defaultHeight(false);

        if (nextPage != -1) {
            builder.addButton(buttonNextPrev("Следующие 5 городов", "menu_next_" + nextPage));
        }
        if (prevPage != -1) {
            builder.addButton(buttonNextPrev("Предыдущие 5 городов", "menu_prev_" + prevPage));
        }

        builder.addButtons(keyboardMenu(ios).getButtons());

        return builder.build();
    }

    public static Keyboard keyboardShareMenu(String url, boolean ios) {
        Keyboard.Builder builder = new Keyboard.Builder();
        builder.defaultHeight(false);

        builder.addButton(buttonShare(SharePlatform.VIBER, url));
        builder.addButton(buttonShare(SharePlatform.VKONTAKTE, url));
        builder.addButton(buttonShare(SharePlatform.FACEBOOK, url));

        builder.addButtons(keyboardMenu(ios).getButtons());

        return builder.build();
    }

    public static Keyboard keyboardPromoPosts(boolean ios) {
        Keyboard.Builder builder = new Keyboard.Builder();
        builder.defaultHeight(false);

        builder.addButton(buttonYesNo("Голосовать", "promo_posts_yes"));
        builder.addButton(buttonYesNo("Не хочу", "promo_posts_no"));

        builder.addButtons(keyboardMenu(ios).getButtons());
        return builder.build();

    }

    private static KeyboardButton buttonMenuSticker() {
        KeyboardButton.Builder builder = new KeyboardButton.Builder();
        builder.width(2).height(1);

        builder.silent(true);
        builder.actionType("open-url").actionBody("https://stickers.viber.com/pages/temnikova?redir=1");

        builder.textSize("large");
        builder.textHAlign("center").textVAlign("center");
        builder.bgColor("#ffffff");

        builder.text("<font color=\"#ff3882\">" + "Стикеры" + "</font>");
        return builder.build();
    }

    private static KeyboardButton buttonMenu(String text, String actionType, String actionBody, String icon) {
        KeyboardButton.Builder builder = new KeyboardButton.Builder();
        builder.width(2).height(1);
        builder.actionType(actionType).actionBody(actionBody);

        if (actionType.equals("open-url")) {
            builder.silent(true);
        }

        builder.textSize("large");
        builder.textHAlign("center").textVAlign("center");
        builder.bgColor("#ffffff");

        builder.text("<font color=\"#ff3882\">" + text + "</font>");
        builder.image(icon);
        return builder.build();
    }

    private static KeyboardButton buttonYesNo(String text, String actionBody) {
        KeyboardButton.Builder builder = new KeyboardButton.Builder();

        builder.width(3).height(1);
        builder.actionType("reply").actionBody(actionBody);

        builder.textSize("large");
        builder.textHAlign("center").textVAlign("center");
        builder.bgColor("#000000");

        builder.text("<font color=\"#ffffff\">" + text + "</font>");

        return builder.build();
    }

    private static KeyboardButton buttonNextPrev(String text, String actionBody) {
        KeyboardButton.Builder builder = new KeyboardButton.Builder();

        builder.width(6).height(1);
        builder.actionType("reply").actionBody(actionBody);

        builder.textSize("large");
        builder.textHAlign("center").textVAlign("center");
        builder.bgColor("#000000");

        builder.text("<font color=\"#ffffff\">" + text + "</font>");

        return builder.build();
    }

    private static KeyboardButton buttonShare(SharePlatform platform, String url) {
        String text = null, openUrl = null;
        switch (platform) {
            case VIBER:
                text = "Viber";
                openUrl = "viber://forward?text=" + url;
                break;
            case FACEBOOK:
                text = "Facebook";
                openUrl = "https://www.facebook.com/sharer.php?u=" + url;
                break;
            case VKONTAKTE:
                text = "ВКонтакте";
                openUrl = "https://vk.com/share.php?url=" + url;
                break;
        }
        return buttonShare(text, platform, openUrl);
    }

    private static KeyboardButton buttonShare(String text, SharePlatform platform, String actionBody) {
        KeyboardButton.Builder builder = new KeyboardButton.Builder();

        if (platform == SharePlatform.VIBER) {
            builder.width(6).height(1);
        } else {
            builder.width(3).height(1);
        }
        builder.actionType("open-url").actionBody(actionBody).silent(true);

        builder.textSize("large");
        builder.textVAlign("middle").textHAlign("center");

        String bgColor = "#ffffff";
        switch (platform) {
            case VIBER: bgColor = "#59267c"; break;
            case FACEBOOK: bgColor = "#3b5998"; break;
            case VKONTAKTE: bgColor = "#45668e"; break;
        }
        builder.bgColor(bgColor);

        builder.text("<font color=\"#ffffff\">" + text + "</font>");
        return builder.build();
    }

    private enum SharePlatform {
        VIBER,
        FACEBOOK,
        VKONTAKTE
    }
}
