package me.wobot.temnikova.api.commands;

import me.wobot.temnikova.api.ApiConfig;
import me.wobot.temnikova.api.commands.base.AbsRequest;
import me.wobot.temnikova.api.commands.base.AbsResponse;
import me.wobot.temnikova.api.model.User;
import me.wobot.temnikova.api.utils.Jsons;
import org.json.JSONObject;

public class GetUserDetails {

    public static class Request extends AbsRequest {

        private final String id;

        public Request(String id) {
            this.id = id;
        }

        @Override
        public String getMethod() {
            return "get_user_details";
        }

        @Override
        public JSONObject toJson() {
            JSONObject result = new JSONObject();
            result.put("auth_token", ApiConfig.TOKEN_ACCOUNT);
            result.put("id", id);
            return result;
        }

        @Override
        public AbsResponse fromResponse(JSONObject answer) {
            return new Response().fromResponse(answer);
        }
    }

    public static class Response extends AbsResponse {

        private User user;
        private int status;
        private String statusMessage;

        @Override
        protected AbsResponse fromResponse(JSONObject response) {
            if (response.has("user")) {
                user = Jsons.safeUser(response, "user");
            }
            if (response.has("status")) {
                status = (int) response.get("status");
            }
            if (response.has("status_message")) {
                statusMessage = (String) response.get("status_message");
            }

            return this;
        }

        public User getUser() {
            return user;
        }

        public int getStatus() {
            return status;
        }

        public String getStatusMessage() {
            return statusMessage;
        }

        @Override
        public String toString() {
            return "Response{" +
                    "user=" + user +
                    ", status=" + status +
                    ", statusMessage='" + statusMessage + '\'' +
                    '}';
        }
    }

}
