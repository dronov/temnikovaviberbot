package me.wobot.temnikova.api.model.messages;

import me.wobot.temnikova.api.ApiConfig;
import me.wobot.temnikova.api.commands.base.JsonSerializer;
import me.wobot.temnikova.api.model.keyboard.Keyboard;
import org.json.JSONObject;

public abstract class AbsMessage implements JsonSerializer {

    private final MessageType type;
    private final Keyboard keyboard;

    AbsMessage(MessageType type) {
        this(type, null);
    }

    AbsMessage(MessageType type, Keyboard keyboard) {
        this.type = type;
        this.keyboard = keyboard;
    }

    @Override
    public void serialize(JSONObject json) {
        json.put("type", type.getValue());
        if (type == MessageType.RICH_MEDIA) {
            json.put("min_api_version", 2);
        }

        // always send avatar and icon url
        JSONObject sender = new JSONObject();
        sender.put("name", ApiConfig.NAME_ACCOUNT);
        sender.put("avatar", ApiConfig.ICON_ACCOUNT);
        json.put("sender", sender);

        if (keyboard != null) {
            JSONObject keyboardJson = new JSONObject();
            keyboard.serialize(keyboardJson);
            json.put("keyboard", keyboardJson);
        }
    }

    public MessageType getType() {
        return type;
    }

    public static AbsMessage from(JSONObject json) {
        if (!json.has("type")) {
            throw new IllegalArgumentException("JSONObject don't have parameter type"); // Only for debug
        }

        String type = json.getString("type");
        switch (type) {
            case "text": return new TextMessage(json);
            default: return null;
        }
    }
}
