package me.wobot.temnikova.api.model;

import org.json.JSONObject;

public class Webhook {

    private final int status;
    private final String statusMessage;

    public Webhook(int status, String statusMessage) {
        this.status = status;
        this.statusMessage = statusMessage;
    }

    public Webhook(JSONObject response) {
        status = response.has("status") ? (int) response.getLong("status") : 0;
        statusMessage = response.has("status_message") ? (String) response.get("status_message") : null;
    }

    public int getStatus() {
        return status;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    @Override
    public String toString() {
        return "Webhook{" +
                "status=" + status +
                ", statusMessage='" + statusMessage + '\'' +
                '}';
    }
}
