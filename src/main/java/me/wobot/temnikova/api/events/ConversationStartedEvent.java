package me.wobot.temnikova.api.events;

import me.wobot.temnikova.api.events.base.Event;
import me.wobot.temnikova.api.model.User;
import me.wobot.temnikova.api.utils.Jsons;
import org.json.JSONObject;

public class ConversationStartedEvent extends Event {

    private final long timestamp;
    private final long messageId;

    private final String context;
    private final User user;

    private final boolean subscribed;

    public ConversationStartedEvent(long timestamp, long messageId, String context, User user, boolean subscribed) {
        this.timestamp = timestamp;
        this.messageId = messageId;
        this.context = context;
        this.user = user;
        this.subscribed = subscribed;
    }

    public ConversationStartedEvent(JSONObject json) {
        timestamp = Jsons.safeLong(json, "timestamp");
        messageId = Jsons.safeLong(json, "message_token");
        context = Jsons.safeString(json, "context");
        user = Jsons.safeUser(json, "user");
        subscribed = Jsons.safeBoolean(json, "subscribed");
    }

    public long getTimestamp() {
        return timestamp;
    }

    public long getMessageId() {
        return messageId;
    }

    public String getContext() {
        return context;
    }

    public User getUser() {
        return user;
    }

    public boolean isSubscribed() {
        return subscribed;
    }

    @Override
    public String toString() {
        return "ConversationStartedEvent{" +
                "timestamp=" + timestamp +
                ", messageId=" + messageId +
                ", context='" + context + '\'' +
                ", user=" + user +
                '}';
    }
}
