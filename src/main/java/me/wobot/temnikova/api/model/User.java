package me.wobot.temnikova.api.model;

import me.wobot.temnikova.api.utils.Jsons;
import org.json.JSONObject;

public class User {

    private final String id;
    private final String name;
    private final String avatarUrl;
    private final String country;
    private final String language;
    private final String primaryDeviceOs;
    private final String deviceType;

    public User(String id, String name, String avatarUrl, String country, String language, String primaryDeviceOs, String deviceType) {
        this.id = id;
        this.name = name;
        this.avatarUrl = avatarUrl;
        this.country = country;
        this.language = language;
        this.primaryDeviceOs = primaryDeviceOs;
        this.deviceType = deviceType;
    }

    public User(JSONObject json) {
        this.id = Jsons.safeString(json, "id");
        this.name = Jsons.safeString(json, "name");
        this.avatarUrl = Jsons.safeString(json, "avatar");
        this.country = Jsons.safeString(json, "country");
        this.language = Jsons.safeString(json, "language");
        this.primaryDeviceOs = Jsons.safeString(json, "primary_device_os");
        this.deviceType = Jsons.safeString(json, "device_type");
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public String getCountry() {
        return country;
    }

    public String getLanguage() {
        return language;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public String getPrimaryDeviceOs() {
        return primaryDeviceOs;
    }

    @Override
    public String toString() {
        return "User{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", avatarUrl='" + avatarUrl + '\'' +
                ", country='" + country + '\'' +
                ", language='" + language + '\'' +
                ", primaryDeviceOs='" + primaryDeviceOs + '\'' +
                ", deviceType='" + deviceType + '\'' +
                '}';
    }
}
