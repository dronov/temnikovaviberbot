package me.wobot.temnikova.service;

import me.wobot.temnikova.Constants;
import me.wobot.temnikova.api.commands.SendMessage;
import me.wobot.temnikova.api.events.ConversationStartedEvent;
import me.wobot.temnikova.api.events.MessageEvent;
import me.wobot.temnikova.api.events.SubscribedEvent;
import me.wobot.temnikova.api.events.UnsubscribedEvent;
import me.wobot.temnikova.api.events.base.Event;
import me.wobot.temnikova.api.model.User;
import me.wobot.temnikova.api.model.keyboard.KeyboardButton;
import me.wobot.temnikova.api.model.keyboard.RichMedia;
import me.wobot.temnikova.api.model.messages.TextMessage;
import me.wobot.temnikova.model.*;
import me.wobot.temnikova.repo.UserRepository;
import me.wobot.temnikova.repo.VoteRepository;
import me.wobot.temnikova.utils.Keyboards;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Component
public class BotServiceImpl implements BotService {

    private final Logger logger = Logger.getLogger(BotServiceImpl.class);

    private static final String RESPONSE_OK = "OK";

    @Autowired
    ApiService apiService;
    @Autowired
    UserRepository userDao;
    @Autowired
    VoteRepository voteDao;

    @Override
    public String onReceived(Event event) {
        if (event instanceof ConversationStartedEvent) {
            return onConversationStarted((ConversationStartedEvent) event);
        } else if (event instanceof SubscribedEvent) {
            return onSubscribed((SubscribedEvent) event);
        } else if (event instanceof UnsubscribedEvent) {
            return onUnsubscribed((UnsubscribedEvent) event);
        } else if (event instanceof MessageEvent) {
            return onMessageEvent((MessageEvent) event);
        } else {
            logger.debug("onReceived, unknown message");
        }
        return RESPONSE_OK;

    }

    private String onConversationStarted(ConversationStartedEvent event) {
        logger.debug("onConversationStarted, userId = " + event.getUser().getId() + ", name = " + event.getUser().getName());
        UserDb userDb = userDao.selectByUserId(event.getUser().getId());

        boolean exists = true;
        if (userDb == null || !userDb.isSubscribed()) {
            logger.debug("onConversationStarted, name = " + event.getUser().getName() + ", userDb is null or UNSUBSCRIBED");
            exists = false;
        }

        if (!exists) {
            logger.debug("onConversationStarted, send start message");

            if (userDb == null) {
                logger.debug("onConversationStarted, userDb is null, try to get details");

                User user = event.getUser();

                User updatedUser = apiService.getUserDetails(user.getId());
                logger.debug("onConversationStarted, getUserDetails = " + updatedUser);
                if (updatedUser != null) {
                    user = updatedUser;
                }

                userDao.save(UserDb.from(user));
            }

            String text = "Привет!\nВливайся в наш праздник. Посети концерты нашего общероссийского тура. Ищи свой город и бери билет, чтобы услышать первым.\n\nКстати, специально для этого тура я записала новый альбом Temnikova 2. Слушал уже?";
            JSONObject respondJson = new SendMessage.Request(event.getUser().getId(),
                    new TextMessage(text, Keyboards.keyboardMenuYesNo(userDb.isIos()))).toJson();

            return respondJson.toString();
        } else {
            logger.debug("onConversationStarted, response ok");
            return RESPONSE_OK;
        }
    }

    private String onSubscribed(SubscribedEvent event) {
        logger.debug("onSubscribed, userId = " + event.getUser().getId() + ", name = " + event.getUser().getName());
        userDao.changeStatus(event.getUser().getId(), UserDb.STATUS_SUBSCRIBED);
//        userDao.save(UserDb.from(event.getUser(), UserDb.STATUS_SUBSCRIBED));

//        apiService.sendTextMessage(event.getUser().getId(), "Ты подписался! Молодец", null);
        // TODO: answer
        return null;
    }

    private String onUnsubscribed(UnsubscribedEvent event) {
        logger.debug("onUnsubscribed, userId = " + event.getUserId());
        userDao.changeStatus(event.getUserId(), UserDb.STATUS_UNSUBSCRIBED);

        return null;
    }

    @Override
    public String onMessageEvent(MessageEvent event) {
        logger.debug("onMessageEvent, event = " + event);
        UserDb userDb = userDao.selectByUserId(event.getUser().getId());
        if (userDb == null) {
            userDb = userDao.save(UserDb.from(event.getUser(), UserDb.STATUS_SUBSCRIBED));
        }
        if (!userDb.isSubscribed()) {
            logger.debug("onMessageEvent, redirect to onSubscribed");
            onSubscribed(new SubscribedEvent(event.getTimestamp(), event.getUser()));
        }

        String action = null;
        if (event.getMessage() instanceof TextMessage) {
            action = ((TextMessage) event.getMessage()).getText();
        }

        logger.info("action = " + action);
        if (action != null) {
            if (action.startsWith("menu_no")) {
                apiService.sendTextMessage(userDb.getUserId(), "Тогда оцени, как тебе?", null);

                RichMedia.Builder builder = new RichMedia.Builder();
                builder.buttonsGroupColumns(6).buttonsGroupRows(7);

                builder.addButton(
                        new KeyboardButton.Builder()
                                .width(6).height(4)
                                .silent(true)
                                .openUrlMediaType("video")
                                .actionType("open-url").actionBody("https://www.youtube.com/watch?v=1TC_Hd5e8is&list=PLHuonje16bozeytOcjxSGFONV5GfATfDt")
                                .image("http://image.ibb.co/ceVVQm/1_demo_temnikova2.jpg")
                                .build()
                ).addButton(
                        new KeyboardButton.Builder()
                                .width(6).height(1)
                                .silent(true)
                                .actionType("none")
                                .textSize("large")
                                .textVAlign("middle")
                                .textHAlign("center")
                                .text("Temnikova 2")
                                .build()
                ).addButton(
                        new KeyboardButton.Builder()
                                .width(6).height(1)
                                .actionType("reply").actionBody("vote_temnikova_2_good")
                                .textSize("large")
                                .textVAlign("middle")
                                .textHAlign("center")
                                .bgColor("#f2f3f3")
                                .text("<font color=\"#ff3882\">" + "Хорошо получилось" + "</font>")
                                .build()
                ).addButton(
                        new KeyboardButton.Builder()
                                .width(6).height(1)
                                .actionType("reply").actionBody("vote_temnikova_2_bad")
                                .textSize("large")
                                .textVAlign("middle")
                                .textHAlign("center")
                                .bgColor("#f2f3f3")
                                .text("<font color=\"#ff3882\">" + "Предыдущий лучше" + "</font>")
                                .build()
                );
                apiService.sendRichMessage(userDb.getUserId(), builder.build(), Keyboards.keyboardMenu(userDb.isIos()));

            } else if (action.equals("menu_yes") || action.equals("vote_temnikova_2_good")) {
                voteDao.save(new VoteDb(userDb.getUserId(), action));

                sendVoteAlbum(userDb, Constants.VOTE_TEMNIKOVA_2, "А какая песня тебе больше понравилась?");

            } else if (action.equals("vote_temnikova_2_bad")) {
                voteDao.save(new VoteDb(userDb.getUserId(), action));

//                apiService.sendTextMessage(userDb.getUserId(), "Как интересно, твой выбор совпадает с 54% фанатов здесь", null);
                sendVoteAlbum(userDb, Constants.VOTE_TEMNIKOVA_1, "А какая песня тебе больше нравится в альбоме Temnikova 1?");

            } else if (action.startsWith("vote_temnikova_2")) {
                voteDao.save(new VoteDb(userDb.getUserId(), action));
                int percentage = getSimilarVotesPercentage("vote_temnikova_2%", action);
                apiService.sendTextMessage(userDb.getUserId(),
                        "Как интересно, твой выбор совпадает с " + percentage + "% фанатов здесь", null);

                sendVoteAlbum(userDb, Constants.VOTE_TEMNIKOVA_1, "А какая песня тебе больше нравится в альбоме Temnikova 1?");

            } else if (action.startsWith("vote_temnikova_1")) {
                voteDao.save(new VoteDb(userDb.getUserId(), action));
                int percentage = getSimilarVotesPercentage("vote_temnikova_1%", action);

                apiService.sendTextMessage(userDb.getUserId(), "Твой выбор совпадает с " + percentage + "% фанатов здесь", null);
                sendConcertTour(userDb, 0, true);
            } else if (action.startsWith("menu_concert_tour")) {
                sendConcertTour(userDb, 0, true);
            } else if (action.startsWith("menu_music")) {
                sendMusic(userDb);
            } else if (action.startsWith("menu_next") || action.startsWith("menu_prev")) {
                int page = Integer.parseInt(action.substring(10));
                sendConcertTour(userDb, page, false);
            } else if (action.equals("music_temnikova_1")) {
                sendMusicAlbum(userDb, Constants.TEMNIKOVA_1, "Список песен альбома Temnikova 1. Слушай демо и покупай", true);
            } else if (action.equals("music_temnikova_2")) {
                sendMusicAlbum(userDb, Constants.TEMNIKOVA_2, "Список песен альбома Temnikova 2. Слушай демо и покупай", true);
            } else if (action.equals("music_temnikova_3")) {
                sendMusicAlbum(userDb, Constants.TEMNIKOVA_3, "Список песен альбома Temnikova III. Слушай демо и покупай", true);
            } else if (action.equals("music_new_clips")) {
                sendMusicAlbum(userDb, Constants.TEMNIKOVA_NEW_CLIPS, "Смотри новые клипы", false);
            } else if (action.startsWith("share")) {
                apiService.sendTextMessage(userDb.getUserId(), "Выбери куда отправить", Keyboards.keyboardShareMenu(ConcertTour.TEMNIKOVA_PA, userDb.isIos()));
            } else if (action.startsWith("promo_tour_posts") || action.startsWith("promo_posts_yes")) {
                if (userDb.getPromoTourVoted() == 1) {
                    apiService.sendTextMessage(userDb.getUserId(),
                            "Ваш голос учтен!\nЗа два часа до концерта ты узнаешь какая движуха будет на этом концерте", Keyboards.keyboardMenu(userDb.isIos()));
                } else {
                    sendPromoAccordingToTime(userDb);
                }
            } else if (action.startsWith("promo_vote")) {
                if (userDb.getPromoTourVoted() == 1) {
                    apiService.sendTextMessage(userDb.getUserId(),
                            "Голос принят!\nЗа два часа до концерта ты узнаешь какая движуха будет на этом концерте", Keyboards.keyboardMenu(userDb.isIos()));
                } else {
                    voteDao.save(new VoteDb(userDb.getUserId(), action));
                    userDao.changePromoVotedStatus(userDb.getUserId(), 1);

                    apiService.sendTextMessage(userDb.getUserId(),
                            "Голос принят!\nЗа два часа до концерта ты узнаешь какая движуха будет на этом концерте", Keyboards.keyboardMenu(userDb.isIos()));
                }
            } else if (action.equals("promo_posts_no")) {
                apiService.sendTextMessage(userDb.getUserId(), "Жаль!\nЧем больше голосов, тем больше ништяков получат гости.\n\nЕсли передумаешь, то можешь найти движуху в туре",
                        Keyboards.keyboardMenu(userDb.isIos()));
            } else if (action.equals("menu_shop")) {
                sendShop(userDb);
            } else if (action.equals("shop_connect_manager")) {
                apiService.sendTextMessage(
                        userDb.getUserId(), "Наш менеджер ответит на любые вопросы: +7 916 591-69-04",
                        Keyboards.keyboardMenu(userDb.isIos())
                );
            }
        }

        return RESPONSE_OK;
    }

    private void sendPromoAccordingToTime(UserDb userDb) {
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("Europe/Moscow"));
        Date now = calendar.getTime();

        List<ConcertTour> filtered = Constants.TOUR_2017_2018
                .stream()
                .filter(concertTour -> concertTour.date.compareTo(now) >= 0)
                .collect(Collectors.toList());

        if (filtered.isEmpty()) {
            apiService.sendTextMessage(userDb.getUserId(),
                    "У нас не осталось больше движух", Keyboards.keyboardMenu(userDb.isIos()));
        }

        ConcertTour tour = filtered.get(0);

        long diff = tour.date.getTime() - now.getTime();

        if (TimeUnit.MILLISECONDS.toHours(diff) <= 24 && TimeUnit.MILLISECONDS.toHours(diff) >= 2) {
            sendPromoPosts(userDb, tour);
        } else {
            if (TimeUnit.MILLISECONDS.toHours(diff) > 24) {
                long hours = TimeUnit.MILLISECONDS.toHours(diff) - 24;
                long minutes = TimeUnit.MILLISECONDS.toMinutes(diff - (hours + 24) * 60 * 60 * 1000);

                apiService.sendTextMessage(userDb.getUserId(), "" +
                                "До начала голосования за движуху в городе " + tour.city + " осталось " + hours + "ч " + minutes + "мин",
                        Keyboards.keyboardMenu(userDb.isIos()));
            } else {
                apiService.sendTextMessage(userDb.getUserId(), getPromoVotedTextForCity(tour),
                        Keyboards.keyboardMenu(userDb.isIos()));
            }
        }
    }

    private int getSimilarVotesPercentage(String allAction, String trackAction) {
        long all = voteDao.countByVoteIdLike(allAction);
        long trackVotes = voteDao.countByVoteIdLike(trackAction);

        logger.info("getSimilarVotesPercentage, all = " + all + ", trackVotes = " + trackVotes);
        double percentage = (trackVotes * 1.0 / all) * 100;
        return (int) percentage;
    }

    private void sendVoteAlbum(UserDb userDb, List<Track> tracks, String introMessage) {
        apiService.sendTextMessage(userDb.getUserId(), introMessage, null);
        RichMedia.Builder builder = new RichMedia.Builder();
        builder.buttonsGroupColumns(6).buttonsGroupRows(7);

        for (Track track : tracks) {
            builder.addButton(
                    new KeyboardButton.Builder()
                            .width(6).height(4)
                            .silent(true)
                            .openUrlMediaType("video")
                            .actionType("open-url").actionBody(track.videoUrl)
                            .image(track.previewUrl)
                            .build()
            ).addButton(
                    new KeyboardButton.Builder()
                            .width(6).height(2)
                            .silent(true)
                            .actionType("none")
                            .textSize("large")
                            .textVAlign("middle")
                            .textHAlign("center")
                            .text(track.name)
                            .build()
            ).addButton(
                    new KeyboardButton.Builder()
                            .width(6).height(1)
                            .actionType("reply").actionBody(track.action)
                            .textSize("large")
                            .textVAlign("middle")
                            .textHAlign("center")
                            .bgColor("#f2f3f3")
                            .text("<font color=\"#ff3882\">" + "Выбрать" + "</font>")
                            .build()
            );
        }

        apiService.sendRichMessage(userDb.getUserId(), builder.build(), Keyboards.keyboardMenu(userDb.isIos()));
    }

    private void sendConcertTour(UserDb userDb, int page, boolean withText) {
        if (withText) {
            apiService.sendTextMessage(userDb.getUserId(), "Мой новый концертный тур скоро начнется! Следи за новостями и смотри, как это было", null);
        }

        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("Europe/Moscow"));
        Date now = calendar.getTime();

        List<ConcertTour> filtered = Constants.TOUR_2017_2018
                .stream()
                .filter(concertTour -> concertTour.date.compareTo(now) >= 0)
                .collect(Collectors.toList());

        List<ConcertTour> currentPage = filtered.subList(page * 5, Math.min(filtered.size(), (page + 1) * 5));

        RichMedia.Builder builder = new RichMedia.Builder();
        builder.buttonsGroupColumns(6).buttonsGroupRows(7);

        ConcertTour videoTour = null;
        if (page == 0) {
            videoTour = new ConcertTour("video_tour", "", "http://image.ibb.co/e1bXY6/Citi_over_preview.jpg",
                    "https://www.youtube.com/playlist?list=PLQ7t2Z77CsoUmXONfShY5oAM3jL-5wVzs", "share_from_video");
            currentPage.add(0, videoTour);
        }

        for (ConcertTour tour : currentPage) {

            if (tour == videoTour) {

                builder.addButton(
                        new KeyboardButton.Builder()
                                .width(6).height(6)
                                .silent(true)
                                .actionType("none")
                                .image(tour.previewUrl)
                                .build()
                )
//                        .addButton(
//                        new KeyboardButton.Builder()
//                                .width(6).height(1)
//                                .actionType("reply").actionBody("promo_tour_posts")
//                                .textSize("large")
//                                .textVAlign("middle")
//                                .textHAlign("center")
//                                .bgColor("#f2f3f3")
//                                .text("<font color=\"#ff3882\">" + "Голосовать за движуху" + "</font>")
//                                .build()
//                )
                    .addButton(
                        new KeyboardButton.Builder()
                                .width(6).height(1)
                                .silent(true)
                                .actionType("open-url").actionBody(tour.buyTickerSite)
                                .textSize("large")
                                .textVAlign("middle")
                                .textHAlign("center")
                                .bgColor("#f2f3f3")
                                .text("<font color=\"#ff3882\">" + "Смотри как это было" + "</font>")
                                .build()
                );

                continue;
            }

            builder.addButton(
                    new KeyboardButton.Builder()
                            .width(6).height(5)
                            .silent(true)
                            .actionType("none")
                            .image(tour.previewUrl)
                            .build()
            ).addButton(
                    new KeyboardButton.Builder()
                            .width(6).height(1)
                            .silent(!tour.isVideoTour())
                            .actionType("open-url").actionBody(tour.buyTickerSite)
                            .textSize("large")
                            .textVAlign("middle")
                            .textHAlign("center")
                            .bgColor("#f2f3f3")
                            .text("<font color=\"#ff3882\">" + "Купить билеты" + "</font>")
                            .build()
            ).addButton(
                    new KeyboardButton.Builder()
                            .width(6).height(1)
                            .actionType("reply").actionBody(tour.shareAction)
                            .textSize("large")
                            .textVAlign("middle")
                            .textHAlign("center")
                            .bgColor("#f2f3f3")
                            .text("<font color=\"#ff3882\">" + "Позвать друзей" + "</font>")
                            .build()
            );
        }

        int l = page * 5;
        int r = (page + 1) * 5;

        int nextPage = -1, prevPage = -1;

        if (l > 0 && l < filtered.size()) {
            prevPage = page - 1;
        }
        if (r < filtered.size()) {
            nextPage = page + 1;
        }

        apiService.sendRichMessage(userDb.getUserId(), builder.build(),
                Keyboards.keyboardConcertTour(nextPage, prevPage, userDb.isIos()));

    }

    private void sendMusic(UserDb userDb) {
        RichMedia.Builder builder = new RichMedia.Builder();
        builder.buttonsGroupColumns(6).buttonsGroupRows(7);

        builder.addButton(
                new KeyboardButton.Builder()
                        .width(6).height(4)
                        .silent(true)
                        .actionType("none")
                        .image("https://i.ibb.co/GxPQDCK/ce5h48ejfn90t0d-preserve-transparency-False-amp-size-1200x1200-amp-size-mode-4.jpg")
                        .build()
        ).addButton(
                new KeyboardButton.Builder()
                        .width(6).height(1)
                        .silent(true)
                        .actionType("none")
                        .textSize("large")
                        .textVAlign("middle")
                        .textHAlign("center")
                        .text("TEMNIKOVA III: Не модные")
                        .build()
        ).addButton(
                new KeyboardButton.Builder()
                        .width(6).height(1)
                        .actionType("reply").actionBody("music_temnikova_3")
                        .textSize("large")
                        .textVAlign("middle")
                        .textHAlign("center")
                        .bgColor("#f2f3f3")
                        .text("<font color=\"#ff3882\">" + "Список песен" + "</font>")
                        .build()
        ).addButton(
                new KeyboardButton.Builder()
                        .width(6).height(1)
                        .actionType("open-url")
                        .silent(true)
                        .actionBody(userDb.isIos() ? Constants.IOS_TEMNIKOVA_3 : Constants.ANDROID_TEMNIKOVA_3)
                        .textSize("large")
                        .textVAlign("middle")
                        .textHAlign("center")
                        .bgColor("#f2f3f3")
                        .text("<font color=\"#ff3882\">" + "Слушать альбом" + "</font>")
                        .build()
        );

        builder.addButton(
                new KeyboardButton.Builder()
                        .width(6).height(4)
                        .silent(true)
                        .actionType("none")
                        .image("https://i.ibb.co/DMd8NGz/image.jpg")
                        .build()
        ).addButton(
                new KeyboardButton.Builder()
                        .width(6).height(1)
                        .silent(true)
                        .actionType("none")
                        .textSize("large")
                        .textVAlign("middle")
                        .textHAlign("center")
                        .text("Новые клипы")
                        .build()
        ).addButton(
                new KeyboardButton.Builder()
                        .width(6).height(1)
                        .actionType("reply").actionBody("music_new_clips")
                        .textSize("large")
                        .textVAlign("middle")
                        .textHAlign("center")
                        .bgColor("#f2f3f3")
                        .text("<font color=\"#ff3882\">" + "Смотреть" + "</font>")
                        .build()
        ).addButton(
                new KeyboardButton.Builder()
                        .width(6).height(1)
                        .actionType("reply")
                        .actionBody("music_temnikova_3")
                        .textSize("large")
                        .textVAlign("middle")
                        .textHAlign("center")
                        .bgColor("#f2f3f3")
                        .text("<font color=\"#ff3882\">" + "Новый альбом" + "</font>")
                        .build()
        );


        builder.addButton(
                new KeyboardButton.Builder()
                        .width(6).height(4)
                        .silent(true)
                        .openUrlMediaType("video")
                        .actionType("open-url").actionBody("https://www.youtube.com/watch?v=X0qURrF6ST0")
                        .image("https://i.ibb.co/GnVx1c9/psvk-cover-final.jpg")
                        .build()
        ).addButton(
                new KeyboardButton.Builder()
                        .width(6).height(2)
                        .silent(true)
                        .actionType("none")
                        .textSize("large")
                        .textVAlign("middle")
                        .textHAlign("center")
                        .text("Под сердцами в кругах")
                        .build()
        ).addButton(
                new KeyboardButton.Builder()
                        .width(6).height(1)
                        .actionType("open-url")
                        .silent(true)
                        .actionBody(userDb.isIos() ? "https://apple.co/2y2LNd" : "https://vk.cc/8y7Cg8")
                        .textSize("large")
                        .textVAlign("middle")
                        .textHAlign("center")
                        .bgColor("#f2f3f3")
                        .text("<font color=\"#ff3882\">" + "Купить песню" + "</font>")
                        .build()
        );


        builder.addButton(
                new KeyboardButton.Builder()
                        .width(6).height(4)
                        .silent(true)
                        .actionType("none")
                        .image("http://image.ibb.co/gNYL86/music_temnikova2.jpg")
                        .build()
        ).addButton(
                new KeyboardButton.Builder()
                        .width(6).height(1)
                        .silent(true)
                        .actionType("none")
                        .textSize("large")
                        .textVAlign("middle")
                        .textHAlign("center")
                        .text("Temnikova 2")
                        .build()
        ).addButton(
                new KeyboardButton.Builder()
                        .width(6).height(1)
                        .actionType("reply").actionBody("music_temnikova_2")
                        .textSize("large")
                        .textVAlign("middle")
                        .textHAlign("center")
                        .bgColor("#f2f3f3")
                        .text("<font color=\"#ff3882\">" + "Список песен" + "</font>")
                        .build()
        ).addButton(
                new KeyboardButton.Builder()
                        .width(6).height(1)
                        .actionType("open-url")
                        .silent(true)
                        .actionBody(userDb.isIos() ? Constants.IOS_TEMNIKOVA_2 : Constants.ANDROID_TEMNIKOVA_2)
                        .textSize("large")
                        .textVAlign("middle")
                        .textHAlign("center")
                        .bgColor("#f2f3f3")
                        .text("<font color=\"#ff3882\">" + "Слушать альбом" + "</font>")
                        .build()
        );

        builder.addButton(
                new KeyboardButton.Builder()
                        .width(6).height(4)
                        .silent(true)
                        .actionType("none")
                        .image("http://image.ibb.co/cUQvam/music_temnikova1_1.jpg")
                        .build()
        ).addButton(
                new KeyboardButton.Builder()
                        .width(6).height(1)
                        .silent(true)
                        .actionType("none")
                        .textSize("large")
                        .textVAlign("middle")
                        .textHAlign("center")
                        .text("Temnikova 1")
                        .build()
        ).addButton(
                new KeyboardButton.Builder()
                        .width(6).height(1)
                        .actionType("reply").actionBody("music_temnikova_1")
                        .textSize("large")
                        .textVAlign("middle")
                        .textHAlign("center")
                        .bgColor("#f2f3f3")
                        .text("<font color=\"#ff3882\">" + "Список песен" + "</font>")
                        .build()
        ).addButton(
                new KeyboardButton.Builder()
                        .width(6).height(1)
                        .actionType("open-url")
                        .silent(true)
                        .actionBody(userDb.isIos() ? Constants.IOS_TEMNIKOVA_1 : Constants.ANDROID_TEMNIKOVA_1)
                        .textSize("large")
                        .textVAlign("middle")
                        .textHAlign("center")
                        .bgColor("#f2f3f3")
                        .text("<font color=\"#ff3882\">" + "Слушать альбом" + "</font>")
                        .build()
        );

        builder.addButton(
                new KeyboardButton.Builder()
                        .width(6).height(4)
                        .silent(true)
                        .actionType("none")
                        .image("http://image.ibb.co/ghsEQw/third.jpg")
                        .build()
        ).addButton(
                new KeyboardButton.Builder()
                        .width(6).height(1)
                        .silent(true)
                        .actionType("none")
                        .textSize("large")
                        .textVAlign("middle")
                        .textHAlign("center")
                        .text("Слушать все песни")
                        .build()
        ).addButton(
                new KeyboardButton.Builder()
                        .width(6).height(1)
                        .actionType("open-url").actionBody("https://itunes.apple.com/ru/artist/elena-temnikova/id661638057?l=en")
                        .silent(true)
                        .textSize("large")
                        .textVAlign("middle")
                        .textHAlign("center")
                        .bgColor("#f2f3f3")
                        .text("<font color=\"#ff3882\">" + "iTunes" + "</font>")
                        .build()
        ).addButton(
                new KeyboardButton.Builder()
                        .width(6).height(1)
                        .actionType("open-url")
                        .silent(true)
                        .actionBody("https://play.google.com/store/music/artist/Елена_Темникова?id=A5acgtt43uxfuo36tyeiu7awi5q")
                        .textSize("large")
                        .textVAlign("middle")
                        .textHAlign("center")
                        .bgColor("#f2f3f3")
                        .text("<font color=\"#ff3882\">" + "Google Play" + "</font>")
                        .build()
        );

        apiService.sendRichMessage(userDb.getUserId(), builder.build(), Keyboards.keyboardMenu(userDb.isIos()));
    }

    private void sendMusicAlbum(UserDb userDb, List<Track> tracks, String introMessage, boolean needToBuy) {
        apiService.sendTextMessage(userDb.getUserId(), introMessage, null);
        RichMedia.Builder builder = new RichMedia.Builder();
        builder.buttonsGroupColumns(6).buttonsGroupRows(needToBuy ? 7 : 6);

        for (Track track : tracks) {
            builder.addButton(
                    new KeyboardButton.Builder()
                            .width(6).height(4)
                            .silent(true)
                            .openUrlMediaType("video")
                            .actionType("open-url").actionBody(track.videoUrl)
                            .image(track.previewUrl)
                            .build()
            ).addButton(
                    new KeyboardButton.Builder()
                            .width(6).height(2)
                            .silent(true)
                            .actionType("none")
                            .textSize("large")
                            .textVAlign("middle")
                            .textHAlign("center")
                            .text(track.name)
                            .build()
            );
            if (needToBuy) {
                builder.addButton(
                        new KeyboardButton.Builder()
                                .width(6).height(1)
                                .actionType("open-url")
                                .silent(true)
                                .actionBody(userDb.isIos() ? track.buyUrlIos : track.buyUrlAndroid)
                                .textSize("large")
                                .textVAlign("middle")
                                .textHAlign("center")
                                .bgColor("#f2f3f3")
                                .text("<font color=\"#ff3882\">" + "Купить песню" + "</font>")
                                .build()
                );
            }
        }

        apiService.sendRichMessage(userDb.getUserId(), builder.build(), Keyboards.keyboardMenu(userDb.isIos()));
    }

    private void sendPromoPosts(UserDb userDb, ConcertTour tour) {
        apiService.sendTextMessage(userDb.getUserId(), "Выбери движуху, которая больше нравится и проголосуй. Помни, у тебя есть только один голос!", null);

        RichMedia.Builder builder = new RichMedia.Builder();
        builder.buttonsGroupColumns(6).buttonsGroupRows(7);

        for (PromoPost promoPost : Constants.PROMO_POSTS) {

            builder.addButton(
                    new KeyboardButton.Builder()
                            .width(6).height(4)
                            .silent(true)
                            .actionType("none")
                            .image(promoPost.previewUrl)
                            .build()
            ).addButton(
                    new KeyboardButton.Builder()
                            .width(6).height(2)
                            .silent(true)
                            .actionType("none")
                            .textSize("small")
                            .textVAlign("middle")
                            .textHAlign("center")
                            .text(promoPost.title + "<br>" + promoPost.description)
                            .build()
            ).addButton(
                    new KeyboardButton.Builder()
                            .width(6).height(1)
                            .actionType("reply").actionBody(promoPost.voteAction + "_" + tour.shareAction)
                            .textSize("large")
                            .textVAlign("middle")
                            .textHAlign("center")
                            .bgColor("#f2f3f3")
                            .text("<font color=\"#ff3882\">" + "Голосовать" + "</font>")
                            .build()
            );

        }

        apiService.sendRichMessage(userDb.getUserId(), builder.build(), Keyboards.keyboardMenu(userDb.isIos()));
    }

    @Override
    public void broadcastStartPromoTour(ConcertTour tour) {
        logger.info("broadcastStartPromoTour START");
        String text = "Привет!\nЗавтра вечером концерт в городе " + tour.city + "! Мы открываем голосование за движуху на сцене.\nПрисоединяйся!";

        List<UserDb> users = userDao.findAllByStatusEquals(UserDb.STATUS_SUBSCRIBED);
        logger.info("broadcastStartPromoTour, users.size() = " + users.size());
        apiService.sendBroadcastMessages(
                users.stream().map(UserDb::getUserId).collect(Collectors.toList()),
                text, Keyboards.keyboardPromoPosts(false));

        logger.info("broadcastStartPromoTour END");
    }

    @Override
    public void broadcastEndPromoTour(ConcertTour tour) {
        logger.info("broadcastEndPromoTour START");

        String text = getPromoVotedTextForCity(tour);

        List<UserDb> users = userDao.findAllByStatusEqualsAndPromoTourVotedEquals(UserDb.STATUS_SUBSCRIBED, 1);
//        users = users.stream().filter(userDb -> userDb.getUserId().equals(ApiConfig.DRONOV_ID)).collect(Collectors.toList());
        logger.info("broadcastEndPromoTour, tour = " + tour.city + ", users.size() = " + users.size());

        for (UserDb userDb : users) {
            userDao.changePromoVotedStatus(userDb.getUserId(), 0);
        }
        apiService.sendBroadcastMessages(
                users.stream().map(UserDb::getUserId).collect(Collectors.toList()),
                text, Keyboards.keyboardMenu(true));

        logger.info("broadcastEndPromoTour END");
    }

    private String getPromoVotedTextForCity(ConcertTour tour) {
        Map<String, Long> results = new HashMap<>();
        for (PromoPost post : Constants.PROMO_POSTS) {
            results.put(post.voteAction, voteDao.countByVoteIdLike(post.voteAction + "_" + tour.shareAction));
        }
        long promo1 = results.get("promo_vote_eshe_pyat_kesov");
        long promo2 = results.get("promo_vote_podkinyt_chydes");
        long promo3 = results.get("promo_vote_v_rassypnyy");
        long promo4 = results.get("promo_vote_avtoprokhodka");

        promo1 = promo1 >= 700 ? promo1 : 0;
        promo2 = promo2 >= 700 ? promo2 : 0;
        promo3 = promo3 >= 700 ? promo3 : 0;
        promo4 = promo4 >= 1500 ? promo4 : 0;

        long max = Math.max(promo1, Math.max(promo2, Math.max(promo3, promo4)));

        String resultText;
        if (max == 0) {
            resultText = "Сегодня на сцене будут радости и гадости из голосования Вконтакте.";
        } else {
            resultText = "Сегодня на сцене будут радости и гадости из голосования Вконтакте.\nА в дополнение к ним ";
            if (max == promo1) {
                resultText += "Еще пять кесов";
            } else if (max == promo2) {
                resultText += "Подкинуть чудес";
            } else if (max == promo3) {
                resultText += "В рассыпную";
            } else if (max == promo4) {
                resultText += "Футболка автопроходка для прохода в гримерку";
            } else {
                resultText += "";
            }

        }

        String text = "Голосование за движуху в городе " + tour.city + " завершено!\n\n" + resultText;
        return text;
    }

    private void sendShop(UserDb userDb) {
        RichMedia.Builder builder = new RichMedia.Builder();
        builder.buttonsGroupColumns(6).buttonsGroupRows(7);

        for (ShopItem shopItem : Constants.SHOP_ITEMS) {

            builder.addButton(
                    new KeyboardButton.Builder()
                            .width(6).height(4)
                            .silent(true)
                            .actionType("none")
                            .image(shopItem.previewUrl)
                            .build()
            ).addButton(
                    new KeyboardButton.Builder()
                            .width(6).height(1)
                            .silent(true)
                            .actionType("none")
                            .textSize("small")
                            .textVAlign("middle")
                            .textHAlign("center")
                            .text(shopItem.description)
                            .build()
            ).addButton(
                    new KeyboardButton.Builder()
                            .width(6).height(1)
                            .actionType("reply").actionBody("shop_connect_manager")
                            .textSize("medium")
                            .textVAlign("middle")
                            .textHAlign("center")
                            .bgColor("#f2f3f3")
                            .text("<font color=\"#ff3882\">" + "Связаться с менеджером" + "</font>")
                            .build()
            ).addButton(
                    new KeyboardButton.Builder()
                            .width(6).height(1)
                            .silent(true)
                            .actionType("open-url").actionBody(shopItem.siteUrl)
                            .textSize("medium")
                            .textVAlign("middle")
                            .textHAlign("center")
                            .bgColor("#f2f3f3")
                            .text("<font color=\"#ff3882\">" + "Перейти в магазин" + "</font>")
                            .build()
            );

        }

        apiService.sendRichMessage(userDb.getUserId(), builder.build(), Keyboards.keyboardMenu(userDb.isIos()));
    }
}
