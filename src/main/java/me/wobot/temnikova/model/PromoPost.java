package me.wobot.temnikova.model;

import me.wobot.temnikova.Constants;

public class PromoPost {

    public final String previewUrl;
    public final String title;
    public final String description;
    public final String voteAction;

    public PromoPost(String title, String description, String voteAction) {
        this(Constants.IOS_TEMNIKOVA_1, title, description, voteAction);
    }

    public PromoPost(String previewUrl, String title, String description, String voteAction) {
        this.previewUrl = previewUrl;
        this.title = title;
        this.description = description;
        this.voteAction = voteAction;
    }
}
