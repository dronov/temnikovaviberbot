package me.wobot.temnikova.api.model.messages;

import me.wobot.temnikova.api.model.keyboard.Keyboard;
import org.json.JSONObject;

public class PhotoMessage extends AbsMessage {

    private final String text;
    private final String photoUrl;

    public PhotoMessage(String text, String photoUrl) {
        this(text, photoUrl, null);
    }

    public PhotoMessage(String text, String photoUrl, Keyboard keyboard) {
        super(MessageType.PICTURE, keyboard);
        this.text = text;
        this.photoUrl = photoUrl;
    }

    public String getText() {
        return text;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    @Override
    public void serialize(JSONObject json) {
        super.serialize(json);
        json.put("text", text);
        json.put("media", photoUrl);
    }
}
