package me.wobot.temnikova.model;

public class Track {

    public final String previewUrl;
    public final String videoUrl;
    public final String name;
    public final String action;
    public final String buyUrlIos;
    public final String buyUrlAndroid;

    public Track(String previewUrl, String videoUrl, String name, String action,
                 String buyUrlIos, String buyUrlAndroid) {
        this.previewUrl = previewUrl;
        this.videoUrl = videoUrl;
        this.name = name;
        this.action = action;
        this.buyUrlIos = buyUrlIos;
        this.buyUrlAndroid = buyUrlAndroid;
    }
}
