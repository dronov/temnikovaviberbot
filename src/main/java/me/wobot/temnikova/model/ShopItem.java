package me.wobot.temnikova.model;

public class ShopItem {

    public final String previewUrl;
    public final String description;
    public final String siteUrl;

    public ShopItem(String previewUrl, String description, String siteUrl) {
        this.previewUrl = previewUrl;
        this.description = description;
        this.siteUrl = siteUrl;
    }
}
