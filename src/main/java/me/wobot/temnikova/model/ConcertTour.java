package me.wobot.temnikova.model;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class ConcertTour {

    public static final String MAIN_SHARE_IMAGE = "http://image.ibb.co/iAYyd6/Share_main.jpg";
    public static final String TEMNIKOVA_PA = "https://chats.viber.com/temnikova/ru";

    public static final DateFormat DATE_FORMAT;

    static {
        DATE_FORMAT = new SimpleDateFormat("dd.MM.yyyy HH:mm");
        DATE_FORMAT.setTimeZone(TimeZone.getTimeZone("Europe/Moscow"));
    }

    public final String city;
    public final Date date;
    public final String previewUrl;
    public final String buyTickerSite;
    public final String shareAction;
    public final String shareUrl;

    public ConcertTour(String city, String date, String previewUrl, String buyTickerSite, String shareAction) {
        this(city, date, previewUrl, buyTickerSite, shareAction, TEMNIKOVA_PA);
    }

    public ConcertTour(String city, String date, String previewUrl, String buyTickerSite, String shareAction,
                       String shareUrl) {
        this.city = city;

        Date formattedDate;
        try {
            formattedDate = DATE_FORMAT.parse(date);
        } catch (ParseException e) {
            formattedDate = null;
        }
        this.date = formattedDate;

        this.previewUrl = previewUrl;
        this.buyTickerSite = buyTickerSite;
        this.shareAction = shareAction;
        this.shareUrl = shareUrl;
    }

    public boolean isVideoTour() {
        return city.equals("video_tour");
    }
}
