package me.wobot.temnikova.model;

import javax.persistence.*;
import java.io.Serializable;

@Table(name = "VoteDb")
@Entity
public class VoteDb implements Serializable {

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;

    @Column(name = "user_id", nullable = false)
    String userId;

    @Column(length = 10000, nullable = false)
    String voteId;


    public VoteDb() {
    }

    public VoteDb(String userId, String voteId) {
        this.userId = userId;
        this.voteId = voteId;
    }
}
