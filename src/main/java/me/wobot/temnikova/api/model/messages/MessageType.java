package me.wobot.temnikova.api.model.messages;

public enum MessageType {

    UNKNOWN("unknown"),
    TEXT("text"),
    PICTURE("picture"),
    URL("url"),
    RICH_MEDIA("rich_media");

    private final String value;

    MessageType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static MessageType from(String value) {
        switch (value) {
            case "text": return TEXT;
            case "picture": return PICTURE;
            case "url": return URL;
            case "rich_media": return RICH_MEDIA;
            default: return UNKNOWN;
        }
    }
}
