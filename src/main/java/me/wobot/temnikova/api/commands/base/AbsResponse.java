package me.wobot.temnikova.api.commands.base;

import org.json.JSONObject;

public abstract class AbsResponse {

    protected abstract AbsResponse fromResponse(JSONObject response);
}
