package me.wobot.temnikova.service;

import me.wobot.temnikova.api.ApiConfig;
import me.wobot.temnikova.api.commands.BroadcastMessage;
import me.wobot.temnikova.api.commands.GetUserDetails;
import me.wobot.temnikova.api.commands.SendMessage;
import me.wobot.temnikova.api.commands.SetWebhook;
import me.wobot.temnikova.api.commands.base.AbsRequest;
import me.wobot.temnikova.api.commands.base.AbsResponse;
import me.wobot.temnikova.api.model.User;
import me.wobot.temnikova.api.model.keyboard.Keyboard;
import me.wobot.temnikova.api.model.keyboard.RichMedia;
import me.wobot.temnikova.api.model.messages.PhotoMessage;
import me.wobot.temnikova.api.model.messages.RichMessage;
import me.wobot.temnikova.api.model.messages.TextMessage;
import me.wobot.temnikova.api.model.messages.UrlMessage;
import me.wobot.temnikova.utils.Lists;
import okhttp3.*;
import org.apache.http.HttpEntity;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.entity.BufferedHttpEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Component
public class ApiService {

    private final Logger logger = Logger.getLogger(ApiService.class);

    private static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    private static final String VIBER_AUTH_TOKEN_HEADER = "X-Viber-Auth-Token";

    private volatile CloseableHttpClient httpClient;
    private final RequestConfig requestConfig;

    private final OkHttpClient okHttpClient = new OkHttpClient();

    public ApiService() {
        httpClient = HttpClientBuilder.create()
                .setSSLHostnameVerifier(new NoopHostnameVerifier())
                .setConnectionTimeToLive(70, TimeUnit.SECONDS)
                .setMaxConnTotal(100)
                .build();

        RequestConfig.Builder configBuilder = RequestConfig.copy(RequestConfig.custom().build());

        requestConfig = configBuilder
                .setSocketTimeout(75 * 1000)
                .setConnectTimeout(75 * 1000)
                .setConnectionRequestTimeout(75 * 1000).build();
    }

    public void setWebhook(String authToken, String url) {
        SetWebhook.Request request = new SetWebhook.Request(authToken, url);
        SetWebhook.Response response = (SetWebhook.Response) sendApiRequest(request);
        if (response != null && response.getStatus() != 0) {
            logger.error("status from setWebhook is " + response.getStatus() + ", message = " + response.getStatusMessage());
        }
    }

    public void sendTextMessage(String userId, String text, Keyboard keyboard) {
        SendMessage.Request request = new SendMessage.Request(userId, new TextMessage(text, keyboard));
        SendMessage.Response response = (SendMessage.Response) sendRequest(request);
        if (response != null && response.getStatus() != 0) {
            logger.info("sendTextMessage: status = " + response.getStatus() + ", message = " +
                    response.getStatusMessage() + ", text = " + text + ", keyboard = " + keyboard);
        }
    }

    public void sendRichMessage(String userId, RichMedia richMedia) {
        sendRichMessage(userId, richMedia, null);
    }

    public void sendRichMessage(String userId, RichMedia richMedia, Keyboard keyboard) {
        SendMessage.Request request = new SendMessage.Request(userId, new RichMessage(richMedia, keyboard));
        SendMessage.Response r = (SendMessage.Response) sendRequest(request);
        if (r != null && r.getStatus() != 0) {
            logger.info("sendTextMessage: status = " + r.getStatus() + ", message = " + r.getStatusMessage() + ", rich = " + richMedia);
        }
    }

    public void sendUrlMessage(String userId, String url, Keyboard keyboard) {
        SendMessage.Request request = new SendMessage.Request(userId, new UrlMessage(url, keyboard));
        SendMessage.Response r = (SendMessage.Response) sendRequest(request);
        if (r != null && r.getStatus() != 0) {
            logger.error("sendUrlMessage: status = " + r.getStatus() + ", message = " + r.getStatusMessage() + ", url = " + url + ", keyboard = " + keyboard);
        }
    }

    public void sendPhotoMessage(String userId, String text, String url, Keyboard keyboard) {
        SendMessage.Request request = new SendMessage.Request(userId, new PhotoMessage(text, url, keyboard));
        SendMessage.Response r = (SendMessage.Response) sendRequest(request);
        if (r != null && r.getStatus() != 0) {
            logger.error("sendUrlMessage: status = " + r.getStatus() + ", message = " + r.getStatusMessage() + ", url = " + url + ", keyboard = " + keyboard);
        }
    }

    public void sendBroadcastMessages(List<String> userIds, String text, Keyboard keyboard) {
        logger.info("sendBroadcastMessages: userIds.count() = " + userIds.size());
        if (userIds.isEmpty()) {
            return;
        }
        TextMessage message = new TextMessage(text, keyboard);

        int broadcastFailures = 0;
        int count = 0;

        List<List<String>> parts = Lists.splitList(userIds, 100);

        long start = System.currentTimeMillis();
        for (int i = 0; i < parts.size(); i++) {
            List<String> part = parts.get(i);

            logger.info("sendBroadcastMessages: sending part = " + (i + 1) + "/" + parts.size());
            BroadcastMessage.Request request = new BroadcastMessage.Request(part, message);
            BroadcastMessage.Response r = (BroadcastMessage.Response) sendRequest(request);
            if (r != null && r.getStatus() != 0) {
                logger.info("sendBroadcastMessages: ERROR " + (i + 1) + "/" + parts.size() + ", response = " + r);

                broadcastFailures++;
                if (broadcastFailures == 5) {
                    broadcastFailures = 0;
                    continue;
                }

                try {
                    Thread.sleep(2000 * broadcastFailures);

                    logger.info("sendBroadcastMessages: sending one more time.......");
                    i--;
                } catch (InterruptedException ignored) {
                }
                continue;
            }

            logger.info("sendBroadcastMessages: SUCCESS = " + (i + 1) + "/" + parts.size());
            broadcastFailures = 0;
            count++;
            if (count == 50) {
                if (TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis() - start) >= 1) {
                    start = System.currentTimeMillis();
                    count = 0;
                } else {
                    try {
                        Thread.sleep(2000);
                    } catch (Exception ignored) {
                    }
                }
            }
        }
    }

    public User getUserDetails(String userId) {
        GetUserDetails.Request request = new GetUserDetails.Request(userId);
        GetUserDetails.Response r = (GetUserDetails.Response) sendRequest(request);
        if (r != null && r.getStatus() != 0) {
            logger.info("getUserDetails: status = " + r.getStatus() + ", message = " + r.getStatusMessage());
            return r.getUser();
        }
        return null;
    }

    private Request createRequest(String endpoint, RequestBody body) {
        return new Request.Builder()
                .url(String.format("%s%s", ApiConfig.URL_VIBER_BASE, endpoint))
                .header(VIBER_AUTH_TOKEN_HEADER, ApiConfig.TOKEN_ACCOUNT)
                .post(body)
                .build();
    }

    private AbsResponse sendRequest(AbsRequest request) {
        final String json = request.toJson().toString();
        final RequestBody body = RequestBody.create(JSON, json);
        final Request okRequest = createRequest(request.getMethod(), body);

        try {
            final Response response = okHttpClient.newCall(okRequest).execute();
            if (response != null && response.body() != null) {
                final String responseBody = response.body().string();
                return request.fromResponse(new JSONObject(responseBody));
            }
        } catch (Exception e) {
            logger.error("sendRequest:" + e.getMessage());
        }
        return null;
    }

    private AbsResponse sendApiRequest(AbsRequest request) {
        String responseContent;
        try {
            String url = ApiConfig.URL_VIBER_BASE + request.getMethod();

            HttpPost httppost = new HttpPost(url);
            httppost.setConfig(requestConfig);
            httppost.addHeader("charset", StandardCharsets.UTF_8.name());
            httppost.setEntity(new StringEntity(request.toJson().toString(), ContentType.APPLICATION_JSON));
            try (CloseableHttpResponse response = httpClient.execute(httppost)) {
                HttpEntity ht = response.getEntity();
                BufferedHttpEntity buf = new BufferedHttpEntity(ht);
                responseContent = EntityUtils.toString(buf, StandardCharsets.UTF_8);
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
            logger.error("sendApiRequest:" + e.getMessage());
            return null;
        }

        JSONObject json = new JSONObject(responseContent);
        return request.fromResponse(json);
    }
}
