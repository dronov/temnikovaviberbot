package me.wobot.temnikova.api.utils;

import me.wobot.temnikova.api.model.User;
import org.json.JSONObject;

public class Jsons {

    public static String safeString(JSONObject json, String key) {
        return json.has(key) ? json.getString(key) : null;
    }

    public static long safeLong(JSONObject json, String key) {
        return json.has(key) ? json.getLong(key) : 0;
    }

    public static boolean safeBoolean(JSONObject json, String key) {
        return json.has(key) ? json.getBoolean(key) : false;
    }

    public static User safeUser(JSONObject json, String key) {
        return json.has(key) ? new User(json.getJSONObject(key)) : null;
    }

}
